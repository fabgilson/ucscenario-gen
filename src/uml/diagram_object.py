from enum import Enum, auto
from typing import List, Set, Dict
from importlib.util import find_spec
import re
import spacy

use_src = find_spec("src")

if use_src:
    from src.storyparser.entity import Entity, EntityType, Property
else:
    from storyparser.entity import Entity, EntityType, Property

nlp = spacy.load('en')
from spacy.lang.en.stop_words import STOP_WORDS



class ObjectType(Enum):
    entity = auto()
    actor = auto()
    control = auto()
    boundary = auto()
    usecase = auto()
    rectangle = auto()
    # represents a property.
    circle = auto()


class BaseObject:

    def __init__(self):
        self.base: "DiagramObject" = None
        self.variations: Dict[str, "DiagramObject"] = {}
        self.all_instances: List["DiagramObject"] = []


class DiagramObject:

    def __init__(self, source=None, object_type=ObjectType.entity):
        self.pretty_name: str = " "
        self.contains: List[DiagramObject] = []
        self.children: Dict[str, DiagramObject] = {}
        self.parents: Dict[str, DiagramObject] = {}
        self.object_type: ObjectType = object_type
        self.ordered_children: List[DiagramObject] = []
        self.explicit: bool = True
        self.descriptors: List[str] = []
        self.ordered_parents = []
        self.root = ""
        self._specialisation = ""
        self.thing = None
        self.is_entity_property = False
        self.ref_ids_notes:List[int] = [] #List id stories where we can find the DiagramObject
        
        if isinstance(source, Entity):
            self._build_from_entity(source)
        elif isinstance(source, Property):
            self._build_from_prop(source)

    def add_ref_id(self,new_id : int):
        self.ref_ids_notes.append(new_id)

    @property
    def specialisation(self):
        """
        The specialisation functions as the key for an object. If two DiagramObjects have the same specialisation they
        should be merged with .merge() before building the diagram.
        :return: specialisation.
        """
        thing_types = [ObjectType.entity, ObjectType.circle, ObjectType.boundary]
        return ("thing" if self.object_type in thing_types else self.object_type.name) + self._specialisation #+("circle" if self.object_type == ObjectType.circle or (self.object_type ==ObjectType.entity and self.is_entity_property) else "")

    def _make_specialisation(self, entity, nested: bool=False):
        """
        Builds the actual specialisation based off of entities. The specialisation property exists for dynamic type
        updates.
        :param entity: to make specialisation from.
        :param nested: Leave this as False.
        :return: A string containing all of the keywords describing this DiagramObject (except for type)
        """
        if nested:
            if entity.entity_type == EntityType.statement:
                return ""
        if entity.entity_type in [EntityType.object, EntityType.interface, EntityType.actor]:
            spec = "".join(entity.description) + (entity.properties[0].formatted_str(pretty=False) if entity.properties else "") + \
                   (entity.lemma if not entity.properties else entity.root)
            if entity.entity_type == EntityType.interface:
                spec += "".join([self._make_specialisation(e, True) for e in entity.relations])
        elif entity.entity_type in [EntityType.extension]:
            spec = "".join([self._make_specialisation(e, True) for e in entity.relations])
        elif entity.entity_type.is_proxy():
            spec = ""
        else:
            spec = entity.lemma + "".join([self._make_specialisation(e, True) for e in entity.relations])
        
        spec = DiagramObject._clean_special_char(spec)
        return spec.replace(" ", "")

    def _clean_special_char(sent):
        """
        Replace or remove every special char not accept by PLAN_UML for the ID
        """
        sent_clean = sent.replace("$","dollar")
        sent_clean = sent_clean.replace("-","moins")
        sent_clean = sent_clean.replace("+","plus")
        sent_clean = sent_clean.replace('"',"quote")
        sent_clean = sent_clean.replace("'","quote")

        return sent_clean




    def _build_from_entity(self, entity: Entity):
        self.root = entity.lemma
        self.pretty_name = re.sub(" +", " ", DiagramObject.make_pretty_name(entity).strip())
        words = self.pretty_name.split(" ")
        chunks = []
        for i in range(0, len(words), 2):
            chunks.append(" ".join(words[i:min(len(words), i + 2)]))
        self.pretty_name = "\\n".join(chunks)
        self.explicit = entity.referenced_explicitly
        self.descriptors = entity.description
        self._specialisation = self._make_specialisation(entity)
        self.is_entity_property = entity.is_property_object

        if entity.entity_type == EntityType.object:
            self.object_type = ObjectType.entity
        elif entity.entity_type == EntityType.statement:
            self.object_type = ObjectType.usecase
        elif entity.entity_type == EntityType.relation:
            self.object_type = ObjectType.control
        elif entity.entity_type == EntityType.actor:
            self.object_type = ObjectType.actor
        elif entity.entity_type == EntityType.interface:
            self.object_type = ObjectType.boundary
        elif entity.entity_type == EntityType.object_property:
            self.object_type = ObjectType.circle

        return self

    def _build_from_prop(self, prop: Property):
        # todo might be correct to make sub properties descriptors so that checking for interfaces works with them.
        words = prop.formatted_str().split(" ")
        chunks = []
        for i in range(0, len(words), 2):
            chunks.append(" ".join(words[i:min(len(words), i + 2)]))
        self.pretty_name = "\\n".join(chunks)
        self._specialisation = prop.formatted_str(pretty=False)
        self.root = prop.name.lower()
        self.explicit = False
    
    @staticmethod
    def make_pretty_name(entity: Entity, include_context: bool=False, in_phrase: bool=False, passive: bool= False, exclude_entities=[]):
       
        word = ""
        tuple_words_index = list(set(DiagramObject.create_list_name(entity, include_context, in_phrase, passive, exclude_entities)))
        tuple_words_index = sorted(tuple_words_index, key=lambda tup: tup[1])
        pretty_name = " ".join([name for name,_ in tuple_words_index])
        if(entity.entity_type != EntityType.relation):
            pretty_name = DiagramObject.clean_sent(pretty_name)
        return pretty_name

    @staticmethod
    def clean_sent(sent):
        """
        Converts all sent words to lem and removes all pronouns or stopwords
        """
        doc = nlp(sent, disable=['parser', 'ner'])
        tokens = [tok.lemma_.lower().strip() for tok in doc if tok.lemma_ != '-PRON-']
        tokens = [tok[0].upper()+tok[1:] for tok in tokens if tok not in STOP_WORDS and tok != ""]
        new_sent = ' '.join(tokens)
        return new_sent
        
    
    def create_list_name(entity: Entity, include_context: bool=False, in_phrase: bool=False, passive: bool= False,exclude_entities=[]):
        """
        Creation of the list of tuples (words, index in the base sentence) that will compose the description of the object diagram  
        """
        list_word = []
        start_of_sentence = in_phrase
        if not in_phrase:
            passive = entity.entity_type == EntityType.statement
        else:
            if entity.entity_type == EntityType.statement or (not passive and entity.entity_type == EntityType.relation):
                return []
        in_phrase = in_phrase or entity.entity_type in [EntityType.relation, EntityType.statement]
        if entity.entity_type.is_proxy():
            return []

        if entity.entity_type == EntityType.extension:
            list_word.append((entity.full_text(include_pretext=True),entity.index))
            for names in [DiagramObject.create_list_name(e, include_context, in_phrase, passive,exclude_entities) for e in entity.relations if e not in exclude_entities and e.entity_type not in [ EntityType.actor,EntityType.interface] ]:
                list_word+=names
        if in_phrase:
            valid = [e for e in entity.relations if e.entity_type not in EntityType.proxies() + [EntityType.statement, EntityType.actor, EntityType.interface ]]
            pivots = [e for e in valid if e.entity_type in [EntityType.object, EntityType.interface]]

            if pivots:
                pivot = pivots[-1]
                text = []
                met = False
                for e in valid:
                    if e is pivot and (entity.description or entity.entity_type == EntityType.relation):
                        met = True
                        text.append([(entity.full_text(include_pretext=entity.entity_type == EntityType.statement or passive),entity.index)])
                    text.append(DiagramObject.create_list_name(e, include_context, in_phrase, passive,exclude_entities))
                    if e is pivot and (not entity.description and not met):
                        if not met:
                            met = True
                            text.append([(entity.full_text(include_pretext=True),entity.index)])
                for names in text:
                    list_word+=names
            else:
                text = [[(entity.full_text(include_pretext=passive or start_of_sentence),entity.index)]] +[DiagramObject.create_list_name(e, include_context, in_phrase, passive,exclude_entities) for e in entity.relations if e not in exclude_entities and e.entity_type not in [EntityType.actor,EntityType.interface]]
                for names in text:
                    list_word+=(names)
        else:
            list_word.append((entity.full_text(include_pretext=False, include_post=False, use_lemma=True),entity.index))
        return list_word

    def string_initializer(self, tabs: int=1, display_all: bool=False) -> str:
        """
        Returns the PlantUML object declaration string for this DiagramObject.
        :param tabs: Indentation level.
        :param display_all: If this entity contains others display_true will box them up.
        :return:
        """
        if self.contains:
            if display_all:
                #TODO Need to find a better solution 
                if(self.pretty_name):
                    init = ("\t" * tabs) + str(self.object_type.name) + " \"" + self.pretty_name + "\" as " + self.specialisation
                else: 
                    init = ("\t" * tabs) + str(self.object_type.name) + " \" test" + self.specialisation + "\" as " + self.specialisation

                init += " {\n"
                for child in self.contains:
                    init += child.string_initializer(tabs + (1 if display_all else 0), display_all=display_all)
                init += ("\t" * tabs) + "}"
                init += "\n"
                return init

            else:
                init = ""
                for child in self.contains:
                    init += child.string_initializer(tabs)
                return init
        else:

            if not display_all and self.object_type == ObjectType.circle:
                return ""

            #TODO Need to find a better solution 
            if(self.pretty_name):
                init = ("\t" * tabs) + str(self.object_type.name) + " \"" + self.pretty_name + "\" as " + self.specialisation
            else:
                init = ("\t" * tabs) + str(self.object_type.name) + " \"" + self.specialisation + "\" as " + self.specialisation

            init += (" #grey" if not self.explicit and display_all and not self.contains else "") + "\n"
            return init

    def add_child(self, child: "DiagramObject"):
        if child.specialisation not in self.children:
            self.children[child.specialisation] = child
            self.ordered_children.append(child)
            child.parents[self.specialisation] = self
            child.ordered_parents.append(self)

    def remove_child(self, child: "DiagramObject"):
        if child.specialisation in self.children:
            del self.children[child.specialisation]
            try:
                self.ordered_children.remove(child)
            except ValueError:
                print(child, "isn't a child of ",self)
                
        if self.specialisation in child.parents:
            del child.parents[self.specialisation]
            child.ordered_parents.remove(self)

    def merge(self, merge_from: "DiagramObject", wipe_merged: bool=True):
        # IMPORTANT - Have to remove children merge_from from BEFORE adding the new one because it's done by string
        # representation. Adding then removing will not add (because it doesn't overwrite) and then remove the one
        # that's supposed to be merged in.
        
        for id in merge_from.ref_ids_notes:
            self.add_ref_id(id)
        
        for child in list(merge_from.ordered_children):
            if child.specialisation not in self.children:
                if wipe_merged:
                    merge_from.remove_child(child)
                self.add_child(child)
        for parent in list(merge_from.ordered_parents):
            if parent.specialisation not in self.parents:
                if wipe_merged:
                    parent.remove_child(merge_from)
                parent.add_child(self)
        if wipe_merged:
            merge_from.disconnect()
        
        if merge_from.object_type == ObjectType.entity and self.object_type == ObjectType.circle:
            self.object_type = ObjectType.entity

        # below are the rules for merge types and explicit. Basically "strong" types like entity override property and
        # if explicit once explicit everywhere.
        if merge_from.object_type == ObjectType.boundary:
            self.object_type = ObjectType.boundary
            for p in self.ordered_parents:
                for sibling in p.ordered_children:
                    if sibling.object_type == ObjectType.boundary and not sibling.explicit and sibling not in [merge_from, self]:
                        self.merge(sibling)

        if self.object_type == ObjectType.boundary and merge_from.object_type in [ObjectType.entity, ObjectType.circle]:
            for p in merge_from.ordered_parents:
                for sibling in p.ordered_children:
                    if sibling.object_type == ObjectType.boundary and not sibling.explicit and sibling not in [merge_from, self]:
                        self.merge(sibling)

        self.explicit = self.explicit or merge_from.explicit

    def disconnect(self):
        """
        Remove all children and parents from this entity.
        """
        for child in list(self.ordered_children):
            self.remove_child(child)

        for parent in list(self.ordered_parents):
            parent.remove_child(self)

    def interface_candidates(self) -> List["DiagramObject"]:
        """
        Returns a list of Diagram Objects that are potential interfaces.
        :return: List of interface candidates.
        """
        if self.object_type == ObjectType.boundary:
            return [self]
        elif self.object_type == ObjectType.entity:
            composite = [e for e in self.ordered_parents + self.ordered_children if self.composite_of(e)]
            if composite:
                return composite
            else:
                return [self]
        elif self.object_type in [ObjectType.control, ObjectType.circle]:
            result = []
            for c in self.ordered_children:
                result += c.interface_candidates()
            return result
        else:
            return []

    def __str__(self):
        return self.string_initializer(tabs=0, display_all=True)[:-1]

    def composite_of(self, other: "DiagramObject"):
        return other.root in self.descriptors

    