#!/bin/bash

kill -9 `cat daemon_pid.txt`
pkill python3.6
rm daemon_pid.txt