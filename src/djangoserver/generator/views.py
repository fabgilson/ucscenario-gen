import time
import os
from json import loads
from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.clickjacking import xframe_options_exempt
from importlib.util import find_spec

use_src = find_spec("src")
if use_src:
    from src.djangoserver.scripts.diagram_generator import DiagramGenerator
else:
    from scripts.diagram_generator import DiagramGenerator


def basic(request):
    return render(request, "generator/basic.html", None)


def full(request):
    return render(request, "generator/full.html", None)


@csrf_exempt
@xframe_options_exempt
def image(request):
    image_id = request.GET.get("id", "")
    max_attempts = 20
    img_exists = None
    while max_attempts:
        if os.path.isfile(os.path.join(os.path.dirname(__file__), "..", "out", image_id + ".png")):
            img_exists = True
            max_attempts = 0
        else:
            time.sleep(1)
            max_attempts -= 1
    if img_exists:
        with open(os.path.join(os.path.dirname(__file__), "..", "out", image_id + ".png"), "rb") as f:
            print("SENT IMAGE RESPONSE")
            return HttpResponse(f.read(), content_type="image/jpeg")
    else:
        return Http404

@csrf_exempt
@xframe_options_exempt
def log(request):
    log_id = request.GET.get("id", "")
    print("GOT A LOG", log_id)
    max_attempts = 20
    log_exists = None
    while max_attempts:
        if os.path.isfile(os.path.join(os.path.dirname(__file__), "..", "out", log_id + "_LOG.txt")):
            log_exists = True
            max_attempts = 0
        else:
            time.sleep(1)
            max_attempts -= 1
    if log_exists:
        with open(os.path.join(os.path.dirname(__file__), "..", "out", log_id + "_LOG.txt"), "rb") as f:
            print("SENT LOG RESPONSE")
            return HttpResponse(f.read(), content_type="application/json;charset=UTF-8")
    else:
        return Http404


@csrf_exempt
def generate(request):
    if request.method == "POST":
        story = request.body.decode("utf-8")
        print("story", story)
        story_id = DiagramGenerator.generate(story)
        return HttpResponse(story_id)
    else:
        return Http404


@csrf_exempt
def generate_multi(request):
    if request.method == "POST":
        stories = loads(request.body.decode("utf-8"))
        print("stories", stories)
        print(request.POST)
        story_id = DiagramGenerator.multi(stories, request.GET.get("display_all").lower().strip() == "true")
        return HttpResponse(story_id)
    else:
        return Http404
