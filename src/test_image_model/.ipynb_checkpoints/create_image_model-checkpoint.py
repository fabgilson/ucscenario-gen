import sys
sys.path.insert(0, "../")
from src.storyparser.tree_parser import TreeParser
from src.uml.diagram import Diagram
from src.util import Logger
from src.storyparser.entity import Entity, EntityType, Property
import os

from src.preprocessing.preprocessing import Preprocessing

import spacy
import subprocess
from importlib.util import find_spec
from datetime import datetime
from typing import List
import time
import traceback

import pandas as pd

plant_uml = os.path.abspath("../plant_uml/plantuml.jar")
nlp = spacy.load("en_core_web_md")
path_save1 = "./djangoserver/out/test1/"
path_save2 = "./djangoserver/out/test2/"

def parse(story: str, logger: Logger, parser: TreeParser=None):
    if not parser:
        parser = TreeParser()
    doc_spacy = nlp(story)
    print("DOC: ",doc_spacy)
    #dependancy_tree = doc_spacy.print_tree()
    parser.entities(doc_spacy, logger,nlp)
    print("parser", parser)


    return parser
def print_log(logger, story_id):
    logger.print_to_file(
        os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out")),
        story_id + "_LOG.txt"
    )


def make(parser, logger, id, stories: List[str]=None, display_all: bool=False,path_save="./"):
    try:

        print("ents : ")
        for e in parser.all_ents:
            e.print()
            if(e.entity_type == EntityType.composite):
                print("COMPOSIT")
                e.get_children_relation()
            print("-----")
        print([n.root for n in parser.all_ents])

        """
        print("Root and lem : ")
        for e in parser.all_ents:
            print(e._root ," , ", e._lemma  )
            print("-----")
        print([e._root for e in parser.all_ents])
        for ent_story, prim_act in parser.ents_by_story:
            print("ACTOR : ")

            for act in prim_act:
                act.print()
                print("---")
            print("OTHER")
            for act in ent_story:
                act.print()
                print("---")
        """
        diagram = Diagram(parser.all_ents, parser.ents_by_story, logger, notes=stories, display_all=display_all)
        diagram_out = os.path.abspath(path_save+id+".txt")
        diagram_file = open(diagram_out, "w+")
        diagram_file.write(diagram.textual_representation)
        diagram_file.close()
        time.sleep(0.2)
        print(subprocess.check_output(["bash", "-c", "java -jar '" + plant_uml + "' '" + diagram_out + "'"]))
    except Exception as e:
        logger.log("error", traceback.format_exc())

def generate(story: str,story_id,path_save ="./"):
    try:
        story_id = story.replace(" ","_")
        print("SOTRY_ID ", story_id)
        print("STORY : ",story)
        parser = TreeParser()

        logger = Logger(story_id)
        story = story.replace("\n", "")
        story_preprocess = Preprocessing().process([story])
        story_split,_ = story_preprocess[0]
        print("story_split ",story_split)
        for s in story_split:
            print("begin :",s)
            p = parse(s, logger, parser=parser)
            print("end :",p)
        make(p, logger, story_id, stories=[story],path_save=path_save,display_all=True)
        return story_id

    except AssertionError:
        add_df_analyse("assertionError",story,"","")
        return story_id

###TODO REMOVE Only for test ###
import pandas as pd
import os
def add_df_analyse(name,us,word,output):
    print("PATH : ",os.listdir())
    if("djangoserver"in os.listdir()):
        path = "./djangoserver/out/test_rule/"+name+".csv"
    else:
        path = "./out/test_rule/"+name+".csv"
    try:
        df = pd.read_csv(path)
    except FileNotFoundError:
        df = pd.DataFrame(columns = ['us' , 'word', 'output' ])
    df = df.append({'us' : us , 'word' : word,'output':output} , ignore_index=True)

    df.to_csv(path,index=False)


path_us = "./test_image_model/us_to_display.csv"
path_save="./djangoserver/out/"
path_save = path_save + sys.argv[1]

all_us = pd.read_csv(path_us)["us"]
for i in range(0,len(all_us)):
    print(all_us[i])
    
    generate(all_us[i],path_save=path_save,story_id=i)

print(path_save)
print(os.listdir("./"))


