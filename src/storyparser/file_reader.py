import yaml
from typing import List


class UserStory:

    def __init__(self, story):
        self.id = story["id"]
        self.title = story["title"].replace("\n", " ")
        self.description = story["description"].replace("\n", " ")
        self.raw = story["raw"].replace("\n", " ")
        self.asWantThat = story["asWantThat"].replace("\n", " ")


class FileReader:

    @staticmethod
    def load(filename) -> List[UserStory]:
        stories: List[UserStory] = []
        with open(filename, 'r') as stream:
            try:
                print(stream)
                data = yaml.safe_load(stream)
                print(data)
                for _, story in data["userStories"].items():
                    stories.append(UserStory(story))
                    print(story)
            except yaml.YAMLError as exc:
                print(exc)

        return stories
