import spacy
from importlib.util import find_spec
import neuralcoref

nlp = spacy.load("en")
nlp.Defaults.stop_words -= {"name"}
neuralcoref.add_to_pipe(nlp)

use_src = find_spec("src")
if use_src:
    from src.preprocessing.preprocessing_enums import PreprocessingEnums
else:
    from preprocessing.preprocessing_enums import PreprocessingEnums


class Preprocessing:
    
    def process(self,all_us):
        all_new_us = []
        for us in all_us:
            all_new_us.append(preprocessing(us))
        return all_new_us 
        
    def clean_symbol(self,all_us):
        all_new_us = []
        for us in all_us:
            new_us, _ = clean_symbol(us,[])
            all_new_us.append(new_us)
        return all_new_us 

def preprocessing(us):
    """
    Preprocess the US with every preprocessing function

    Return news us after preprocessing ([String]) with the preprocessing list
    """
    results = []
    states = []

    us,states = process_us_to_us(us,clean_spec_char,states,PreprocessingEnums.clean_spec_char)
    us,states = process_us_to_us(us,clean_multiple_space,states,PreprocessingEnums.clean_multiple_space)
    us,states = process_us_to_us(us,lower_us,states,PreprocessingEnums.lower_us)
    us,states = process_us_to_us(us,remove_unknow_actor,states,PreprocessingEnums.remove_unknow_actor)
    us,states = process_us_to_us(us,add_control,states,PreprocessingEnums.add_control)
    
    us,states = clean_symbol(us,states)
    
    us,states = process_us_to_us(us,replace_coref,states,PreprocessingEnums.replace_coref)


    split = split_us(us)
    
    list_new_sec_part ,states = process_us_to_multi_us(split[1],split_by_conj,states,PreprocessingEnums.split_by_conj)
    for np_2 in list_new_sec_part:
        list_new_sec_part_2 ,states = process_us_to_multi_us(np_2,split_by_conj_with_compl,states,PreprocessingEnums.split_by_conj_with_compl)
        for new_part_2 in  list_new_sec_part_2:
            list_new_st_part ,states = process_us_to_multi_us(split[0],split_multi_actors,states,PreprocessingEnums.split_multi_actors)
            for new_part_1 in  list_new_st_part:
                new_us = unsplit_us([ new_part_1,new_part_2,split[2]])
                results.append(new_us)
    print(results)
    return results, states

def clean_symbol(us,states):
    us,states = process_us_to_us(us,replace_slash_by_or,states,PreprocessingEnums.replace_slash_by_or)
    
    #us,states = process_us_to_us(us,replace_hyphen_by_underscore,states,PreprocessingEnums.replace_hyphen_by_underscore)
    #us,states = process_us_to_us(us,replace_plus_by_underscore,states,PreprocessingEnums.replace_plus_by_underscore)
    #us,states = process_us_to_us(us,replace_double_quote_by_simple_quote,states,PreprocessingEnums.replace_double_quote_by_simple_quote)
    us,states = process_us_to_us(us,remove_comma_before_conj,states,PreprocessingEnums.remove_comma_before_conj)

    return us,states

def process_us_to_us(old_us,process,states,new_process_state):
    """
    if the US is modifie by process:
        add new preprocessing state in states 
        return process US , new states
    else return us , states
    """
    new_us = process(old_us)
    if(old_us != new_us):
        if(new_process_state not in states):
            states.append(new_process_state)
    return new_us,states

def process_us_to_multi_us(old_us,process,states,new_process_state):
    """
    Applies a pre-processing function to US. 
    If it has had an effect, the information is added to states.
    Returns the modified US and the new states 
    """
    new_us = process(old_us)

    if(old_us != new_us[-1]):
        if(new_process_state not in states):
            states.append(new_process_state)
    return new_us,states


def clean_multiple_space(us):
    """
    return US with only one space between two tokens
    """
    last_us = ""
    new_us = us
    while new_us != last_us:
        last_us = str(new_us)
        new_us = new_us.replace("  "," ")
    return new_us
    


def lower_us(us):
    """
    replace every word with one upper case by his lower version
    exemple:
    I want to Improve my Skills => I want to improve my skills
    I want to improve my SuperSkill => I want to improve my SuperSkill
    """
    doc = nlp(us)
    for tok in doc:
        shape = tok.shape_
        nb_up = len([c for c in shape if(c=="X")])
        if(nb_up==1 and len(shape)>1):
            us = us.replace(tok.text,tok.text.lower())
    return us 

def unsplit_us(split):
    result = split[0]+ " " +split[1]+ " " + split[2]
    result = clean_multiple_space(result)
    return result.strip()

def split_us(us):
    """
    us: String that respects the format "AS ... ,I ... (SO THAT ...)"
    return ["AS part","I WANT part","SO THAT part"]
    """
    idx_cut = us.lower().find("so that")
    
    if(idx_cut == -1):
        third = ""
        other = us
    else:  
        third = us[idx_cut:]
        other = us[:idx_cut]
 
    idx_cut = other.lower().find(", i ")
    if idx_cut == -1:
        idx_cut = other.lower().find(", i'd")
    add = 2
    if(idx_cut == -1):
        idx_cut = other.lower().find(",i ")
        if idx_cut == -1:
            idx_cut = other.lower().find(",i'd")
        add = 1
        
        if(idx_cut == -1):
            idx_cut = other.lower().find(" i ")
            if idx_cut == -1:
                idx_cut = other.lower().find(" i'd")
            add = 0
    first = other[:idx_cut+1]    
    second = other[idx_cut + add:]
    
    if(first.lower().find("as ") == -1):
        first = ""
    
    return [first.strip(),second.strip(),third.strip()]

def remove_unknow_actor(us):
    """
    Exemple : "As ..., I and my team want to ..." => "As ..., I want to ..."
    """
    split = split_us(us)
    doc = nlp(split[1])
    for i in range(0,len(doc)):
        if(doc[i].text == "I"):
            if(doc[i+1].text in ["and","or","/"]):
                for j in range(i,len(doc)):
                    if(doc[j].text in ["want","would","need","'d'"]):
                        return unsplit_us([split[0],doc[:i+1].text + " "+ doc[j:].text,split[2]])
            else : 
                return us
    else : 
        return us

def replace_slash_by_or(us):
    new = us.replace("/"," or ")
    new = new.replace("|"," or ")
    new = clean_multiple_space(new)
    return new

def replace_hyphen_by_underscore(us):
    new = us.replace("-","_")
    return new

def replace_plus_by_underscore(us):
    new = us.replace("+","_")
    return new
def replace_double_quote_by_simple_quote(us):
    new = us.replace('"',"'")
    return new    

def add_control(us):
    """
    modifie : second part US

    if not control : add "to be provided with" to US
    return us
    
    ex: "... I want a profile ..." => "... I want to be provided with a profile ..."
    """
    split = split_us(us)
    second_part = split[1]

    words_check = ["want ","would like ","'d like","need"]

    if (second_part.find("to ") == -1):
        for w in words_check:
            idx = second_part.find(w)
            if idx != -1:
                new_second_part = second_part[:idx+len(w)]+ " to be provided with "+second_part[idx+len(w):]
                new_second_part = clean_multiple_space(new_second_part)
                split[1] = new_second_part 
                return unsplit_us(split)

    return us

def remove_comma_before_conj(us):
    """
    Remove them","" placed before a CC because it causes problems for SpaCy parsing.
    Returns the modified US if necessary, 
    otherwise return the US without anything
    """
    doc = nlp(us)
    mod = False #True if we remove a comma
    tokens = []
    for i in range(0,len(doc)):
        if(doc[i].text == "," and (i+1<len(doc)) and doc[i+1].dep_ == "cc"):
            mod = True
        else:
            tokens.append(doc[i].text)
    if mod:
        return ' '.join(tokens)
    else : 
        return us

def check_coref(doc):
    """
    Returns true if there is a detected coref and the words to replace are "it", "her", "them", "him" 
    and that for "her" and "it" are objectives.
    """
    if doc._.coref_clusters: 
        for cluster in doc._.coref_clusters:
            for coref in cluster[1:]:
                if coref.text not in ["him","them","it","her"]:
                    return False
                if coref.text in ["it","her"]:
                    for tok in coref:
                        if (tok.dep_ not in ["dobj", "pobj", "obj","iobj"] ):
                            return False
        return True
    else:
        return False

def replace_coref(us):
    """
    Returns the US with the coreferences replaced by their reference word    
    """
    split = split_us(us)
    doc = nlp(split[1])
    if(check_coref(doc)):
        split[1]= doc._.coref_resolved
    return unsplit_us(split)




##### SPLIT conj ##### V AND V COMPL
def split_verb_conj(sent):
    """
    cc = ["and","or",",","/"]
    
    if sent like "P1 to VB1 cc VB2 P2"
        return [["VB1","VB2"]]
        
    if sent like "P1 to VB1 cc VB2 P2 VB3 cc VB4 P3"
        return [["VB1","VB2"],["VB3","VB4"]]
        
    if sent like "P1 to VB1 cc VB2 cc VB3 P2"
        return [["VB1","VB2","VB3"]]
        
    if sent like "P1 to VB1 prt cc VB2 P2"
        return [["VB1 prt","VB2"]] 
        
    if sent like "P1 to can VB1 cc VB2 P2"
        return [["can VB1","VB2"]]  
    
    else return []
    """
    doc = nlp(sent)
    block = False
    result = []
    for i in range(0,len(doc)):
        if (doc[i].text in ["and","or",",","/"]):
            vb_before = False
            vb_after = False
            if(len(doc)>i+1):
                vb_after = doc[i+1].pos_=="VERB"
                if(vb_after):
                    vb_after_str = doc[i+1].text
                
            if(not vb_after and len(doc)>i+2):
                vb_after = doc[i-1].tag_== "MD" and doc[i+2].pos_=="VERB"
                if(vb_after):
                    vb_after_str = doc[i-1].text + " " + doc[i+1].text 
              
            vb_before = doc[i-1].pos_=="VERB"
            id_vb_before = i-1
                         
            if(not vb_before):
                vb_before = (doc[i-1].dep_== "prt" and doc[i-2].pos_=="VERB")
                id_vb_before = i-2
                if(vb_before):
                    vb_before_str =  doc[id_vb_before].text + " " + doc[id_vb_before+1].text 
            else:
                vb_before_str = doc[id_vb_before].text
                
            if(vb_before and doc[id_vb_before-1].tag_=="MD"):
                vb_before_str = doc[id_vb_before-1].text +" "+vb_before_str
                id_vb_before = id_vb_before - 1
            
            if(vb_after and vb_before):
                if(not block):
                    if(doc[id_vb_before-1].text=="to"):
                        result.append([vb_before_str])
                        block=True
                else:
                    result[-1].append(vb_before_str)  
                        
        #VERB after CC        
        else:
            if(block):
                id_end_vb=-1
                if(doc[i].pos_=="VERB" and not(doc[i].tag_=="MD")):
                    id_end_vb = i
                    vb_str = doc[i].text
                    
                elif(len(doc)>i+1 and doc[i].tag_=="MD" and doc[i+1].pos_=="VERB"):
                    id_end_vb = i+1
                    vb_str = doc[i].text+" "+doc[i+1].text
                
                if(id_end_vb >=0 and len(doc)>id_end_vb+1):
                    if(doc[id_end_vb+1].dep_ == "prt"):
                        vb_str = vb_str + " " + doc[id_end_vb+1].text
                        id_end_vb = id_end_vb + 1


                    if(not(len(doc)>id_end_vb+1 and doc[id_end_vb+1].text in ["and","or",",","/"])):
                        result[-1].append(vb_str)
                        block = False
                                                          
    return result

def split_by_conj(sent):
    """
    sent = second part User story
    cc = ["and","or",",","/"]
    
    if sent like "P1 to VB1 cc VB2 P2"
        return ["P1 to VB1 P2","P1 to VB2 P2"]
        
    if sent like "P1 to VB1 cc VB2 cc VB3 P2"
        return ["P1 to VB1 P2","P1 to VB2 P2","P1 to VB3 P2"]
        
    if sent like "P1 to VB1 cc VB2 P2 VB3 cc VB4 P3"
        return ["P1 to VB1 P2 VB3 P3","P1 to VB1 P2 VB4 P3",
                "P1 to VB2 P2 VB3 P3","P1 to VB1 P2 VB4 P3"]
    
    else return [sent]
    """
    block_verbes = split_verb_conj(sent)
    new_sents  = []
    idx_begin = len(sent)
    idx_end = 0
    add_len = 0
    if(len(block_verbes)>0):
        verbes = block_verbes[0]
        for v in verbes:
            idx = sent.find(v)
            if(idx_begin > idx):
                idx_begin = idx
            if(idx_end < idx):
                idx_end = idx
                add_len = len(v)
        for v in verbes:
            new_sent = sent[:idx_begin]+" "+ v+" "+sent[idx_end+add_len:]
            new_sents.append(new_sent.replace("  "," "))
        return new_sents
    else :
        return [sent]
    
##### SPLIT conj ##### V COMPL AND V COMPL

def check_VB_parent(doc):
    for i in range(0,len(doc)):
        if (doc[i].tag_ == "CC" or doc[i].tag_ == "," or doc[i].text == "/"):
            if(len(doc)>i+1):
                if(doc[i+1].tag_ == "VB"):
                    ancestors=[i for i in doc[i+1].ancestors]
                    if(ancestors):
                        parent = ancestors[0]
                        if(parent.tag_ =="VB"):
                            x = [parent] + check_VB_parent(doc[i+1:])
                            return x
                    
    if(len(doc)>0 and doc[0].tag_ == "VB"):
        return [doc[0]]
    return []

def split_by_conj_with_compl(sent):
    """
    sent = second part User story
    cc = ["and","or",",","/"]
    
    if sent like "P1 to VB1 COMPL1 cc VB2 COMPL2 P2"
        return ["P1 to VB1 COMPL1 P2","P1 to VB2 COMPL2 P2"]
         
    if sent like "P1 to VB1 COMPL1 cc VB2 COMPL2 cc VB3 COMPL3 P2"
        return ["P1 to VB1 COML1 P2","P1 to VB2 COMPL2 P2","P1 to VB3 COMPL3 P2"]
        
    
    else return [sent]
    """
    
    doc = nlp(sent)
    verbes = check_VB_parent(doc)
    idx_begin_all = len(doc)
    
    if(len(verbes)>1):
        sub_sent = []
        for idx_v in range(0,len(verbes)):
            idx_end = 0
            idx_begin_vb = len(doc)
            for i in range(0,len(doc)):
                if(verbes[idx_v] == doc[i]):
                    idx_begin_vb = i 
                    if(idx_begin_vb<idx_begin_all):
                        idx_begin_all = idx_begin_vb
                    for j in range(i,len(doc)):
                        if(idx_v+1 >= len(verbes)):
                            idx_end = len(doc)
                        else:
                            if(verbes[idx_v+1] == doc[j]):
                                idx_end = j-1
            sub_sent.append(doc[:idx_begin_all].text + " "+ doc[idx_begin_vb:idx_end].text)          
                        
        return sub_sent
    else:
        return [sent]

def split_multi_actors(sent):
    """
    split actors in the first part (As ...)
    exemple : As a manager or users I want ...
        - As a manager I want ...
        - As users I want ...
    """
    doc = nlp(sent)
    if len(sent)>0 :
        if doc[-1].text != "," :
            sent = ''.join([sent, ', '])
            doc = nlp(sent)
        idx_splt =[]
        print(sent)
        for token in doc:
            if (token.tag_ == "CC" or token.tag_ == "," or token.text == "/"):
                idx_splt.append(token.i)
        if len(idx_splt)<=0:
            return [sent]
        else:
            sub_sent = []
            sub_sent.append(doc[:idx_splt[0]].text)
            for i in range(len(idx_splt)-1):
                sub_sent.append("as "+  doc[idx_splt[i]+1:idx_splt[i+1]].text)
            return sub_sent
    else :
        return [sent]
        
def clean_spec_char(us):
    doc = nlp(us)
    new_us = us[:]
    for token in doc :
        if token.text == '�':
            idx = new_us.find('�')
            l_nbor = token.nbor(-1)
            r_nbor = token.nbor()
            if ((token.pos_ == 'PROPN' and l_nbor.pos_ == 'VERB' and r_nbor.pos_ == 'PROPN') 
            or (token.pos_ == 'VERB' and l_nbor.pos_ == 'PRON') 
            or (l_nbor.pos_ == 'NOUN' and l_nbor.text[-1] == 's' and r_nbor.pos_ =='NOUN') 
            or (r_nbor.pos_ == 'PART' and l_nbor.pos_ == 'NOUN' and r_nbor.text == 's')) :
                new_us = new_us[:idx] + '\'' + new_us[idx+1:]
            else :
                new_us = new_us[:idx] + ' ' + new_us[idx+1:]
    return new_us

    