import React,{Component} from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import MainNavBar from"./components/nav_bar"
import SimpleUS from"./containers/simple_us"
import MultiUS from"./containers/multi_us"
import ViewByStory from"./containers/view_by_us"
import MultiSplit from"./containers/multi_split_us"






class App extends Component {

  constructor(props){
    super(props)
    this.state={}
  }
  

  render(){

  return (
    
    <div>
      <MainNavBar/>
    <Router>
      <div className="global">
        <Route exact path="/" component={Home} />
        <Route path="/single" component={SimpleUS} />
        <Route path="/multi" component={MultiUS} />
        <Route path="/byview" component={ViewByStory} />
        <Route path="/multi_split" component={MultiSplit} />

      </div>
    </Router>
    </div>
  );
}
}

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Topic({ match }) {
  return <h3>Requested Param: {match.params.id}</h3>;
}

function Topics({ match }) {
  return (
    <div>
      <h2>Topics</h2>

      <ul>
        <li>
          <Link to={`${match.url}/components`}>Components</Link>
        </li>
        <li>
          <Link to={`${match.url}/props-v-state`}>Props v. State</Link>
        </li>
      </ul>

      <Route path={`${match.path}/:id`} component={Topic} />
      <Route
        exact
        path={match.path}
        render={() => <h3>Please select a topic.</h3>}
      />
    </div>
  );
}



export default App;