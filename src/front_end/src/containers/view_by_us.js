import React,{Component} from "react";
import Diagram from"../containers/main_diagram"
import { Form,Button,DropdownButton,Dropdown,Col,Row} from 'react-bootstrap';
import axios from 'axios'

class ViewByStory extends Component {
    constructor(props){
        super(props)
        this.state={
                    listTypeView:["Actor","Entity","Boundary"],
                    listDiagramByView:{
                        "Actor":{currentMainId:"",listDiagram:[]},
                        "Entity":{currentMainId:"",listDiagram:[]},
                        "Boundary":{currentMainId:"",listDiagram:[]}
                    },
                    currentTypeView:"Actor",
                    currentItem:"",
                    currentDropDownView:"Select type of view"
                }
      }

    
    render(){
        console.log("on render ",this.state.currentTypeView)
        return (
            <div>
                <h1>
                    Viewpoint based on an element                
                </h1>
                <Form className="form_size">
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                        <Form.Control placeholder={"As ..., I want ..., so that ..."} as="textarea" rows="3" onChange={this.handleChange.bind(this)}/>
                    </Form.Group>
                    <Row>
                        <Col><Button variant="dark"  onClick={() => {this.switchDiagram()}}>
                                Submit
                            </Button>
                        </Col>
                        <Col>
                            <DropdownButton  
                            title={this.state.currentDropDownView}
                            variant={"Secondary".toLowerCase()}
                            id={`dropdown-variants-Secondary`}
                            key={"TypeView"}
                            className="drop_down_button"
                            onChange={(e)=>
                                console.log(e)
                                }
                            >
                                <Dropdown.Item onClick={()=>this.setState({currentTypeView:"Actor",currentDropDownView:"Actor"})} eventKey="1">Actor</Dropdown.Item>
                                <Dropdown.Item onClick={()=>this.setState({currentTypeView:"Entity",currentDropDownView:"Entity"})} eventKey="2">Entity</Dropdown.Item>
                                <Dropdown.Item onClick={()=>this.setState({currentTypeView:"Boundary",currentDropDownView:"Boundary"})} eventKey="3">Boundary</Dropdown.Item>
                        </DropdownButton>
                        {this.renderDropDownItem()}
                        </Col>
                       
                    </Row>
                    
                    
                </Form>
                {this.renderSelectDiagram()}
            </div>
        )
    }

    renderDropDownItem(){
        
        console.log("renderDropDownItem ",this.state.listDiagramByView)

        if(this.state.listDiagramByView[this.state.currentTypeView].listDiagram.length >0){
                return (<DropdownButton
                            title={"Select item"}
                            variant={"Secondary".toLowerCase()}
                            id={`dropdown-variants-Secondary-`}
                            key={"SelectItem"}
                            className="drop_down_button"
                            onChange={(e)=>
                                console.log(e)
                                }
                            >
                            {this.state.listDiagramByView[this.state.currentTypeView].listDiagram.map((diagram)=>{
                                var item = diagram.name
                                return <Dropdown.Item onClick={()=>this.setState({currentItem:item})} eventKey={item}>{item.toString().replace(/\\/, "/---/").replace("/---/n"," ")}</Dropdown.Item>

                            })}
        
                        </DropdownButton>)
        }
        
                        
    }

    renderSelectDiagram(){
        
        var actualDiagram =  this.state.listDiagramByView[this.state.currentTypeView].listDiagram.filter((diagram) => diagram.name == this.state.currentItem)[0];
        var currentMainId = this.state.listDiagramByView[this.state.currentTypeView].currentMainId
        console.log("renderSelectDiagram ",actualDiagram)
        if(actualDiagram){
            var diagram = {
                title : " : "+actualDiagram["name"],
                main_id:currentMainId,
                id:actualDiagram["id"],
                type:"view_"+this.state.currentTypeView.toLowerCase(),
                us:actualDiagram.list_us,
                plant_uml:actualDiagram["plant_uml"]
            }
            return  <div><Diagram diagram={diagram} key={actualDiagram["id"]}/></div>          
        }
        
     
    }

    renderListDiagram(){
        var actualDiagram = this.state.listDiagramByView[this.state.currentTypeView]
        console.log("actualDiagram ",actualDiagram)
        return(
        <div>
            {actualDiagram.listDiagram.map((d)=>{
                console.log(d)
                var diagram = {
                    title : d["name"],
                    main_id:actualDiagram.currentMainId,
                    id:d["id"],
                    type:"view_"+this.state.currentTypeView,
                    us:this.state.currentStories
                }
                return <Diagram diagram={diagram} key={d["id"]}/>
            })}
        </div>)
        
    }


    switchDiagram(){ 

        
        var URL_CREATE_DIAGRAMS = 'http://127.0.0.1:5000/create_view_'
        var URL_CURRENT_VIEW =  URL_CREATE_DIAGRAMS+this.state.currentTypeView.toLowerCase()  
        
        var actualView = this.state.currentTypeView
        axios.post(URL_CURRENT_VIEW,{us:this.state.currentStories})
          .then( (response) =>{
            console.log(response);

            
            
            var listDiagramByView={
                "Actor":{currentMainId:"",listDiagram:[]},
                "Entity":{currentMainId:"",listDiagram:[]},
                "Boundary":{currentMainId:"",listDiagram:[]}
            }

            listDiagramByView[actualView]={currentMainId:response["data"]["main_id"],listDiagram:response["data"]["list_view"]}
            console.log("Down load : ",listDiagramByView)

            
            this.setState({listDiagramByView:listDiagramByView})


          })
          .catch(function (error) {
            console.log(error);
          });

        this.state.listTypeView.map((typeView)=>{
            if(typeView!=actualView){
                var URL_CURRENT_VIEW =  URL_CREATE_DIAGRAMS+typeView.toLowerCase()
                console.log(URL_CURRENT_VIEW)
                axios.post(URL_CURRENT_VIEW,{us:this.state.currentStories})
                .then( (response) =>{
                    console.log(response);

                var listDiagramByView = this.state.listDiagramByView

                listDiagramByView[typeView]={currentMainId:response["data"]["main_id"],listDiagram:response["data"]["list_view"]}

                
                this.setState({listDiagramByView:listDiagramByView})


            })
            .catch(function (error) {
                console.log(error);
            });


            }
        })


    }

    handleChange(event) {
        console.log( event.target.value)
        this.setState({ currentStories: event.target.value })
    }


}



export default ViewByStory;