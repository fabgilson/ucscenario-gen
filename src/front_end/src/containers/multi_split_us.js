import React,{Component} from "react";
import Diagram from"../containers/main_diagram"
import { Form,Button,Col,Row} from 'react-bootstrap';
import axios from 'axios'

class MultiSplit extends Component {
    constructor(props){
        super(props)
        this.state={currentDiagram :{},currentStories:"",listDiagram:[],currentMainId:""}
      }

    
    render(){
        return (
            <div>
                <h1>
                    Robustness diagram split into subdiagrams
                </h1>
                <Form className="form_size">
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                        <Form.Control placeholder="As ..., I want ... so that ..." as="textarea" rows="3" onChange={this.handleChange.bind(this)}/>
                    </Form.Group>
                    <Row>
                        <Col><Button variant="dark"  onClick={() => {this.switchDiagram()}}>
                                Submit
                            </Button>
                        </Col>
                        <Col>
                        </Col>
                       
                    </Row>
                    
                    
                </Form>
                {this.renderListDiagram()}
            </div>
        )
    }

    renderListDiagram(){
        return(
        <div>
            {this.state.listDiagram.map((d)=>{
                console.log("rep: ",d)
                console.log(d["name"])
                var diagram = {
                    title : d["name"],
                    main_id:this.state.currentMainId,
                    id:d["id"],
                    type:"multi_split",
                    us:d["list_us"],
                    plant_uml:d["plant_uml"]
                }
                return <Diagram diagram={diagram} key={d["id"]}/>
            })}
        </div>)
        
    }

    switchDiagram(){ 
        var URL_CREATE_DIAGRAMS = 'http://127.0.0.1:5000/create_multi_split'
        axios.post(URL_CREATE_DIAGRAMS,{us:this.state.currentStories})
          .then( (response) =>{
            console.log(response);

            var diagram = {
                title : this.state.currentStories,
                main_id:response["data"]["main_id"],
                type:"multi",
                us:this.state.currentStories
              }
            this.setState({currentDiagram:diagram})
            this.setState({currentMainId:response["data"]["main_id"]})
            this.setState({listDiagram:response["data"]["list_view"]})


          })
          .catch(function (error) {
            console.log(error);
          });


    }

    handleChange(event) {
        console.log( event.target.value)
        this.setState({ currentStories: event.target.value })
    }


}



export default MultiSplit;