import React,{Component} from "react";
import Diagram from"../containers/main_diagram"
import { Form,Button} from 'react-bootstrap';
import axios from 'axios'


class MultiUS extends Component {
    constructor(props){
        super(props)
        this.state={currentDiagram :{},currentStories:""}
      }

    
    render(){
        console.log("on render")
        return (
            <div>
                <h1>
                    Multiple user stories
                </h1>

                <Form className="form_size">
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                        <Form.Control placeholder="As ..., I want ... so that ..." as="textarea" rows="3" onChange={this.handleChange.bind(this)}/>
                    </Form.Group>
                    <Button variant="dark"  onClick={() => {this.switchDiagram()}}>
                        Submit
                    </Button>
                </Form>
                <Diagram diagram={this.state.currentDiagram}/>
            </div>
        )
    }

    switchDiagram(){          
        axios.post('http://127.0.0.1:5000/create_multi',{us:this.state.currentStories})
          .then( (response) =>{
            console.log(response);
            var diagram = {
                title : "",
                id:response["data"]["id"],
                type:"multi",
                plant_uml:response["data"]["plant_uml"],
                us:this.state.currentStories.split("\n")
              }
            this.setState({currentDiagram:diagram})
          })
          .catch(function (error) {
            console.log(error);
          });


    }

    handleChange(event) {
        console.log( event.target.value)
        this.setState({ currentStories: event.target.value })
    }


}



export default MultiUS;