# Welcome to the repo of the "Automatic Generation of robustness diagrams from User Stories"

This (set of) tools is in beta version and still under improvement. The doc is rather minimal, but feel free to try it. 

## What's in there
`|_ bash_file` all scritps for installation

`|_ data` annotated user stories (from Dalpiaz [https://data.mendeley.com/datasets/7zbk8zsd8y/1])

`|_ notebooks` jupyter notebooks with various tests and scripts

`|_ plant_uml` PlantUML related resources (jar and guide)

`|_ src` source files of the generation tool

## Run it the easy way:
### Install everything (you need python 3 first)
```
bash ./bash_file/install.sh
```

### Run Api Layer (server)
```
bash ./bash_file/run_api.sh
```

* Running on http://127.0.0.1:5000/

### Run FrontEnd React
```
bash ./bash_file/run_front_end.sh
```
* Running on http://localhost:3000


## How to install the server and client manually
###Install python requirement :
```
 pip install requirement.txt
```

### Download SpaCy model 
```
 python -m spacy download en_core_web_md 
 python -m spacy download en
```

### Commands to execute to deploy the flask model generation API
```
 cd ./src/api/
 export FLASK_APP=api.py
 flask run
 * Running on http://127.0.0.1:5000/
```
### Commands to execute to deploy the React frontend
```
 cd ./src/front_end/
 npm install
 npm start
```

### Legacy Django server (v.0.1) [ASWEC'18 version)
```
 cd src/djangoserver/
 python3 manage.py runserver 0:2000
```

