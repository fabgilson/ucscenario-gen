
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Container" as thingcontainer
	entity "Location Information" as thinglocationinformation #grey
	actor "Archivist" as actorarchivist
	boundary "Container\nInterface" as thingcontainerinterface #grey
	control "Assign Location\nInformation To\nA Container" as controlassignlocationinformationcontainer

	thingcontainer <.. thinglocationinformation
	actorarchivist --- thingcontainerinterface
	thinglocationinformation --- controlassignlocationinformationcontainer
	thingcontainer --- controlassignlocationinformationcontainer
	thingcontainerinterface --> controlassignlocationinformationcontainer

@enduml