
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Resource" as thingresource
	entity "Keyboard ,\nBased Navigation" as thingkeyboardbasednavigation
	entity "Digital" as thingdigital
	actor "Archivist" as actorarchivist
	boundary "Object Hierarchy" as thingobjecthierarchyusekeyboardbasednavigationarchivist #grey
	boundary "Digital\nInterface" as thingdigitalinterface #grey
	control "Rearrange The\nResource" as controlrearrangeresourceobjecthierarchyusekeyboardbasednavigationarchivist
	control "Rearrange The\nResource Digital" as controlrearrangeresourcedigital
	control "Use Keyboard,\nBased Navigation" as controlusekeyboardbasednavigation

	thingdigital <.. thingresource
	actorarchivist --- thingobjecthierarchyusekeyboardbasednavigationarchivist
	actorarchivist --- thingdigitalinterface
	thingresource --- controlrearrangeresourceobjecthierarchyusekeyboardbasednavigationarchivist
	thingobjecthierarchyusekeyboardbasednavigationarchivist --> controlrearrangeresourceobjecthierarchyusekeyboardbasednavigationarchivist
	thingdigital --- controlrearrangeresourcedigital
	thingdigitalinterface --> controlrearrangeresourcedigital
	thingresource --- controlrearrangeresourcedigital
	controlrearrangeresourceobjecthierarchyusekeyboardbasednavigationarchivist --> controlusekeyboardbasednavigation
	thingkeyboardbasednavigation --- controlusekeyboardbasednavigation
	thingobjecthierarchyusekeyboardbasednavigationarchivist --> controlusekeyboardbasednavigation

@enduml