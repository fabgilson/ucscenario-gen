
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Topic Mapping" as thingtopicmapping #grey
	entity "Help Url" as thinghelpurl
	actor "Application Administrator" as actorapplicationadministrator
	boundary "Url\nInterface" as thingurlinterface #grey
	boundary "Mapping\nInterface" as thingmappinginterface #grey
	control "Configure The\nCenter Help\nUrl" as controlconfigurecenterhelpURL
	control "Configure Topic\nMapping" as controlconfiguretopicmapping

	actorapplicationadministrator --- thingurlinterface
	actorapplicationadministrator --- thingmappinginterface
	thinghelpurl --- controlconfigurecenterhelpURL
	thingurlinterface --> controlconfigurecenterhelpURL
	thingtopicmapping --- controlconfiguretopicmapping
	thingmappinginterface --> controlconfiguretopicmapping

@enduml