
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Instance" as thinginstance
	entity "Resource" as thingresource
	actor "Archivist" as actorarchivist
	circle "Component" as thingcomponent #grey
	boundary "Resource\nInterface" as thingresourceinterface #grey
	control "Associate An\nInstance With\nResource Component" as controlassociateinstanceresourcecomponent
	control "Associate An\nInstance With\nA Resource" as controlassociateinstanceresource

	thingresource <.. thinginstance
	thingcomponent <.. thinginstance
	thingresource *-- thingcomponent
	actorarchivist --- thingresourceinterface
	thingcomponent --- controlassociateinstanceresourcecomponent
	thingresourceinterface --> controlassociateinstanceresourcecomponent
	thinginstance --- controlassociateinstanceresourcecomponent
	thinginstance --- controlassociateinstanceresource
	thingresource --- controlassociateinstanceresource
	thingresourceinterface --> controlassociateinstanceresource

@enduml