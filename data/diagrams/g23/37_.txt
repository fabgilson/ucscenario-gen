
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Record" as thingrecord
	entity "Change" as thingchange
	actor "Archivist" as actorarchivist
	boundary "Record\nInterface" as thingrecordinterface #grey
	control "That Editing" as controleditoverwritechangerecordthat
	control "Overwrite Changes\nTo A\nRecord" as controloverwritechangerecord

	thingrecord <.. thingchange
	actorarchivist --- thingrecordinterface
	thingrecordinterface --> controleditoverwritechangerecordthat
	controleditoverwritechangerecordthat --> controloverwritechangerecord
	thingchange --- controloverwritechangerecord
	thingrecord --- controloverwritechangerecord
	thingrecordinterface --> controloverwritechangerecord

@enduml