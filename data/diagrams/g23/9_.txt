
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Marcxml Records" as thingmarcxmlrecords #grey
	entity "Resource" as thingresource
	actor "Archivist" as actorarchivist
	boundary "Records\nInterface" as thingrecordsinterface #grey
	control "Import Resources\nFrom Marcxml\nRecords" as controlimportresourcemarcxmlrecords

	thingmarcxmlrecords <.. thingresource
	actorarchivist --- thingrecordsinterface
	thingresource --- controlimportresourcemarcxmlrecords
	thingmarcxmlrecords --- controlimportresourcemarcxmlrecords
	thingrecordsinterface --> controlimportresourcemarcxmlrecords

@enduml