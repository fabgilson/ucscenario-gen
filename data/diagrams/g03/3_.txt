
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "User Account" as thinguseraccount
	actor "Customer" as actorcustomer
	boundary "Account\nInterface" as thingaccountinterface #grey
	control "Create A\nPortal, Customer\nUser Account" as controlcreateportalcustomeruseraccount

	actorcustomer --- thingaccountinterface
	thinguseraccount --- controlcreateportalcustomeruseraccount
	thingaccountinterface --> controlcreateportalcustomeruseraccount

@enduml