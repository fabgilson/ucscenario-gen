
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Planning Study" as thingplanningstudy #grey
	entity "Plan Amendments" as thingplanamendments #grey
	actor "Planning ,\nStaff Member" as actorplanningstaffmember
	boundary "Study\nInterface" as thingstudyinterface #grey
	boundary "Amendments\nInterface" as thingamendmentsinterface #grey
	control "Conduct Planning\nStudy" as controlconductplanningstudy
	control "Conduct Plan\nAmendments" as controlconductplanamendments
	control "Track Planning\nStudy" as controltrackplanningstudy
	control "Track Plan\nAmendments" as controltrackplanamendments

	actorplanningstaffmember --- thingstudyinterface
	actorplanningstaffmember --- thingamendmentsinterface
	thingplanningstudy --- controlconductplanningstudy
	thingstudyinterface --> controlconductplanningstudy
	thingplanamendments --- controlconductplanamendments
	thingamendmentsinterface --> controlconductplanamendments
	thingplanningstudy --- controltrackplanningstudy
	thingstudyinterface --> controltrackplanningstudy
	thingplanamendments --- controltrackplanamendments
	thingamendmentsinterface --> controltrackplanamendments

@enduml