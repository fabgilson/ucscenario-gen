
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Nametag" as thingnametag
	entity "Camper" as thingcamper
	actor "Camp Administrator" as actorcampadministrator
	boundary "Camper\nInterface" as thingcamperinterface #grey
	control "Create Nametags\nFor Campers" as controlcreatenametag

	thingcamper <.. thingnametag
	actorcampadministrator --- thingcamperinterface
	thingnametag --- controlcreatenametag
	thingcamper --- controlcreatenametag
	thingcamperinterface --> controlcreatenametag

@enduml