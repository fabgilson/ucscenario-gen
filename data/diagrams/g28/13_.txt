
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Broadcaster Logos" as thingbroadcasterlogos #grey
	entity "Video" as thingvideo
	entity "Related Information" as thingrelatedinformation
	actor "User" as actoruser
	boundary "Video\nInterface" as thingvideointerface #grey
	control "Identify Broadcaster\nLogos In\nVideos" as controlidentifybroadcasterlogosvideo
	control "Receive Related\nInformation About\nBroadcaster Logos\nIn Videos" as controlreceiverelatedinformation

	thingvideo <.. thingbroadcasterlogos
	thingbroadcasterlogos <.. thingrelatedinformation
	actoruser --- thingvideointerface
	thingbroadcasterlogos --- controlidentifybroadcasterlogosvideo
	thingvideo --- controlidentifybroadcasterlogosvideo
	thingvideointerface --> controlidentifybroadcasterlogosvideo
	thingrelatedinformation --- controlreceiverelatedinformation
	thingbroadcasterlogos --- controlreceiverelatedinformation
	thingvideo --- controlreceiverelatedinformation
	thingvideointerface --> controlreceiverelatedinformation

@enduml