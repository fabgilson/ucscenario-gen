
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Picture" as thingpicture
	entity "Comment" as thingcomment
	entity "Talk" as thingtalk
	entity "Automate Analysis" as thingautomatedanalysis
	actor "Zooniverse Admin" as actorzooniverseadmin
	control "Assess" as controlassess

	thingtalk <.. thingcomment

@enduml