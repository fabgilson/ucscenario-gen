
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Plankton Species" as thingplanktonspecies #grey
	entity "Plankton Portal\nImages" as thingplanktonportalimages #grey
	actor "Zooniverse Admin" as actorzooniverseadmin
	circle "Automatic Preclassification" as thingautomaticpreclassification
	boundary "Images\nInterface" as thingimagesinterface #grey
	control "Perform Automatic\nPreclassification Of\nPlankton Species\nIn Plankton\nPortal Images" as controlperformautomaticpreclassification

	thingplanktonportalimages <.. thingplanktonspecies
	thingplanktonspecies *-- thingautomaticpreclassification
	actorzooniverseadmin --- thingimagesinterface
	thingautomaticpreclassification --- controlperformautomaticpreclassification
	thingplanktonspecies --- controlperformautomaticpreclassification
	thingplanktonportalimages --- controlperformautomaticpreclassification
	thingimagesinterface --> controlperformautomaticpreclassification

@enduml