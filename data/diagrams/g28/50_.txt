
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Volunteer" as thingvolunteer
	actor "Zooniverse Admin" as actorzooniverseadmin
	boundary "Volunteer\nInterface" as thingvolunteerinterface #grey
	control "Educate A\nVolunteer" as controleducatevolunteer
	control "Know" as controlknoweducatevolunteer

	actorzooniverseadmin --- thingvolunteerinterface
	thingvolunteer --- controleducatevolunteer
	controlknoweducatevolunteer --> controleducatevolunteer
	thingvolunteerinterface --> controleducatevolunteer
	thingvolunteerinterface --> controlknoweducatevolunteer

@enduml