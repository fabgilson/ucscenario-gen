
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "License Statement" as thinglicensestatement
	entity "Right" as thingright
	actor "Archivist" as actorarchivist
	boundary "Right\nInterface" as thingrightinterface #grey
	boundary "Statement\nInterface" as thingstatementinterface #grey
	control "Apply Right" as controlapplyright
	control "Apply A\nLicense Statement" as controlapplylicensestatement

	actorarchivist --- thingrightinterface
	actorarchivist --- thingstatementinterface
	thingright --- controlapplyright
	thingrightinterface --> controlapplyright
	thinglicensestatement --- controlapplylicensestatement
	thingstatementinterface --> controlapplylicensestatement

@enduml