
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Access Derivatives" as thingaccessderivatives #grey
	entity "Digital Object" as thingdigitalobject
	actor "Archivist" as actorarchivist
	boundary "Object\nInterface" as thingobjectinterface #grey
	control "Create Access\nDerivatives Of\nDigital Objects" as controlcreateaccessderivatives

	thingdigitalobject <.. thingaccessderivatives
	actorarchivist --- thingobjectinterface
	thingaccessderivatives --- controlcreateaccessderivatives
	thingdigitalobject --- controlcreateaccessderivatives
	thingobjectinterface --> controlcreateaccessderivatives

@enduml