
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "File" as thingfile
	entity "Collection" as thingcollection
	actor "Archivist" as actorarchivist
	boundary "Collection\nInterface" as thingcollectioninterface #grey
	control "Browse Through\nFiles In\nA Collection" as controlbrowsefilecollection

	thingcollection <.. thingfile
	actorarchivist --- thingcollectioninterface
	thingfile --- controlbrowsefilecollection
	thingcollection --- controlbrowsefilecollection
	thingcollectioninterface --> controlbrowsefilecollection

@enduml