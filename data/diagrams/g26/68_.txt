
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Working Papers" as thingworkingpapers
	actor "Researcher" as actorresearcher
	boundary "Papers\nInterface" as thingpapersinterface #grey
	control "Access Tagged\nWorking Papers" as controlaccesstaggedworkingpapers

	actorresearcher --- thingpapersinterface
	thingworkingpapers --- controlaccesstaggedworkingpapers
	thingpapersinterface --> controlaccesstaggedworkingpapers

@enduml