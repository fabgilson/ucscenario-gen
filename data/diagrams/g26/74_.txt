
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Textual ,\nDigitize ,\nAvailable Document" as thingtextualdigitizedavailabledocument
	entity "Ocred Text" as thingocredtext #grey
	actor "Researcher" as actorresearcher
	boundary "Document\nInterface" as thingdocumentinterface #grey
	control "Search Ocred\nText In\nAll Textual,\nDigitized, Available\nDocuments" as controlsearchocredtexttextualdigitizedavailabledocument

	thingtextualdigitizedavailabledocument <.. thingocredtext
	actorresearcher --- thingdocumentinterface
	thingocredtext --- controlsearchocredtexttextualdigitizedavailabledocument
	thingtextualdigitizedavailabledocument --- controlsearchocredtexttextualdigitizedavailabledocument
	thingdocumentinterface --> controlsearchocredtexttextualdigitizedavailabledocument

@enduml