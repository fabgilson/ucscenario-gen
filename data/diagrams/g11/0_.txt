
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Cloud .\nGov" as thingcloud.gov
	entity "Beta" as thingbeta
	actor "Team Member" as actorteammember
	circle "Iteration" as thingfirstiteration
	boundary "Cloud.Gov\nInterface" as thingcloudgovinterface #grey
	control "See The\nFirst Iteration\nOf Beta\nUp On\nCloud.Gov" as controlseefirstiteration

	thingcloud.gov <.. thingbeta
	thingbeta *-- thingfirstiteration
	actorteammember --- thingcloudgovinterface
	thingfirstiteration --- controlseefirstiteration
	thingbeta --- controlseefirstiteration
	thingcloud.gov --- controlseefirstiteration
	thingcloudgovinterface --> controlseefirstiteration

@enduml