
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Content Guide" as thingcontentguide #grey
	entity "Style Guide" as thingstyleguide #grey
	entity "Design Guide" as thingdesignguide #grey
	actor "Designer" as actordesigner
	boundary "Guide\nInterface" as thingguideinterface #grey
	control "Have A\nStyle Guide" as controlhavestyleguide
	control "Have Design\nGuide" as controlhavedesignguide
	control "Have Content\nGuide" as controlhavecontentguide

	actordesigner --- thingguideinterface
	thingstyleguide --- controlhavestyleguide
	thingguideinterface --> controlhavestyleguide
	thingdesignguide --- controlhavedesignguide
	thingguideinterface --> controlhavedesignguide
	thingcontentguide --- controlhavecontentguide
	thingguideinterface --> controlhavecontentguide

@enduml