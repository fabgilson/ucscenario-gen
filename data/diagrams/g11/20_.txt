
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "User Research" as thinguserresearch #grey
	entity "Plan" as thingplan
	entity "Time" as thingtime
	actor "Team Member" as actorteammember
	boundary "Research\nInterface" as thingresearchinterface #grey
	control "Understand How\nTime With\nPlans Around\nThe User\nResearch" as controlunderstandtimeplan

	thinguserresearch <.. thingplan
	thingplan <.. thingtime
	actorteammember --- thingresearchinterface
	thingtime --- controlunderstandtimeplan
	thingplan --- controlunderstandtimeplan
	thinguserresearch --- controlunderstandtimeplan
	thingresearchinterface --> controlunderstandtimeplan

@enduml