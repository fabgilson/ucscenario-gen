
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Conjunction" as thingconjunction
	entity "New Cms" as thingnewCMS
	entity "Level Strategy\nRecommendations" as thinglevelstrategyrecommendations
	actor "Nsf Employee" as actornsfemployee
	boundary "Recommendations\nInterface" as thingrecommendationsinterface #grey
	boundary "Cms\nInterface" as thingcmsinterface #grey
	control "Have High\nLevel Strategy\nRecommendations" as controlhavehighlevelstrategyrecommendationsimplementconjunction
	control "Implement In\nConjunction With\nThe New\nCms" as controlimplementconjunction

	thingnewCMS <.. thingconjunction
	actornsfemployee --- thingrecommendationsinterface
	actornsfemployee --- thingcmsinterface
	thinglevelstrategyrecommendations --- controlhavehighlevelstrategyrecommendationsimplementconjunction
	thingrecommendationsinterface --> controlhavehighlevelstrategyrecommendationsimplementconjunction
	controlhavehighlevelstrategyrecommendationsimplementconjunction --> controlimplementconjunction
	thingconjunction --- controlimplementconjunction
	thingnewCMS --- controlimplementconjunction
	thingcmsinterface --> controlimplementconjunction

@enduml