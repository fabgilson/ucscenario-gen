
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Design Direction" as thingdesigndirection #grey
	entity "Social ,\nBeta Page" as thingsocialbetapage
	actor "Team Member" as actorteammember
	boundary "Page\nInterface" as thingpageinterface #grey
	control "Determine A\nDesign Direction\nFor The\nSocial, Beta\nPage" as controldeterminedesigndirection

	thingsocialbetapage <.. thingdesigndirection
	actorteammember --- thingpageinterface
	thingdesigndirection --- controldeterminedesigndirection
	thingsocialbetapage --- controldeterminedesigndirection
	thingpageinterface --> controldeterminedesigndirection

@enduml