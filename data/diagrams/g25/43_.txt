
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Object" as thingobject
	entity "Component" as thingcomponent
	actor "Repository Manager" as actorrepositorymanager
	boundary "Object\nInterface" as thingobjectinterface #grey
	control "License Pertains\nTo The\nComponent From\nThe Object\nAs A\nWhole" as controlpertaincomponentobjectwhole
	control "Indicate" as controlindicatepertaincomponentobjectwhole

	thingobject <.. thingcomponent
	actorrepositorymanager --- thingobjectinterface
	thingcomponent --- controlpertaincomponentobjectwhole
	thingobject --- controlpertaincomponentobjectwhole
	controlindicatepertaincomponentobjectwhole --> controlpertaincomponentobjectwhole
	thingobjectinterface --> controlpertaincomponentobjectwhole
	thingobjectinterface --> controlindicatepertaincomponentobjectwhole

@enduml