
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Preservation Managers" as thingpreservationmanagers #grey
	entity "Checksum" as thingchecksum
	entity "Checksum Method" as thingchecksummethod
	entity "Object" as thinggivenobject
	entity "Master File" as thingmasterfile #grey
	entity "Manager" as thingmanager #grey
	actor "Dams Manager" as actordamsmanager
	boundary "Checksum\nInterface" as thingchecksuminterface #grey
	boundary "Object\nInterface" as thingobjectinterface #grey
	control "Indicate To\nManagers Checksum" as controlindicatemanagerchecksum
	control "Indicate To\nPreservation Managers\nThe Checksum\nMethod For\nEach Master\nFile For\nA Given\nObject" as controlindicatepreservationmanagerschecksummethod

	thingchecksummethod <.. thingpreservationmanagers
	thingmasterfile <.. thingchecksummethod
	thinggivenobject <.. thingmasterfile
	thingchecksum <.. thingmanager
	actordamsmanager --- thingchecksuminterface
	actordamsmanager --- thingobjectinterface
	thingmanager --- controlindicatemanagerchecksum
	thingchecksum --- controlindicatemanagerchecksum
	thingchecksuminterface --> controlindicatemanagerchecksum
	thingpreservationmanagers --- controlindicatepreservationmanagerschecksummethod
	thingchecksummethod --- controlindicatepreservationmanagerschecksummethod
	thingmasterfile --- controlindicatepreservationmanagerschecksummethod
	thinggivenobject --- controlindicatepreservationmanagerschecksummethod
	thingobjectinterface --> controlindicatepreservationmanagerschecksummethod

@enduml