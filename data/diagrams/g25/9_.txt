
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity ", Aspect" as thingothermanyaspect
	entity "Inclusive Date" as thinginclusivedate
	entity "Collection" as thingcollection
	entity "Bulk" as thingbulk
	entity "Title" as thingtitle
	actor "Repository Manager" as actorrepositorymanager
	boundary "Date\nInterface" as thingdateinterface #grey
	boundary "Title\nInterface" as thingtitleinterface #grey
	boundary "Aspect\nInterface" as thingaspectinterface #grey
	boundary "Bulk\nInterface" as thingbulkinterface #grey
	control "Describe A\nCollection Including\nInclusive Date" as controldescribecollectioninclusivedate
	control "Describe A\nCollection Including\nTitle" as controldescribecollection
	control "Describe Other,\nMany Aspect" as controldescribeothermanyaspect
	control "Describe A\nCollection Including\nBulk" as controldescribecollectionbulk

	thingtitle <.. thingcollection
	thinginclusivedate <.. thingcollection
	thingbulk <.. thingcollection
	actorrepositorymanager --- thingdateinterface
	actorrepositorymanager --- thingtitleinterface
	actorrepositorymanager --- thingaspectinterface
	actorrepositorymanager --- thingbulkinterface
	thinginclusivedate --- controldescribecollectioninclusivedate
	thingdateinterface --> controldescribecollectioninclusivedate
	thingcollection --- controldescribecollectioninclusivedate
	thingcollection --- controldescribecollection
	thingtitle --- controldescribecollection
	thingtitleinterface --> controldescribecollection
	thingothermanyaspect --- controldescribeothermanyaspect
	thingaspectinterface --> controldescribeothermanyaspect
	thingbulk --- controldescribecollectionbulk
	thingbulkinterface --> controldescribecollectionbulk
	thingcollection --- controldescribecollectionbulk

@enduml