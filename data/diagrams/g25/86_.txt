
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Pertinent Statute" as thingpertinentstatute
	entity "Object" as thingobject
	actor "Dams Manager" as actordamsmanager
	circle "Component" as thingcomponent #grey
	boundary "Object\nInterface" as thingobjectinterface #grey
	control "Know The\nPertinent Statutes\nTo An\nObject" as controlknowpertinentstatute
	control "Know The\nPertinent Statutes\nTo Object\nComponent" as controlknowpertinentstatuteobjectcomponent

	thingobject <.. thingpertinentstatute
	thingcomponent <.. thingpertinentstatute
	thingobject *-- thingcomponent
	actordamsmanager --- thingobjectinterface
	thingpertinentstatute --- controlknowpertinentstatute
	thingobject --- controlknowpertinentstatute
	thingobjectinterface --> controlknowpertinentstatute
	thingcomponent --- controlknowpertinentstatuteobjectcomponent
	thingpertinentstatute --- controlknowpertinentstatuteobjectcomponent
	thingobjectinterface --> controlknowpertinentstatuteobjectcomponent

@enduml