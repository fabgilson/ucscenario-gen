
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Deposit Groups" as thingdepositgroups #grey
	entity "Individual People" as thingindividualpeople
	actor "Digital ,\nRecords Archivist" as actordigitalrecordsarchivist
	boundary "Method" as thingmethodaddindividualpeopledepositgroupsdigitalrecordsarchivist
	control "Add Individual\nPeople From\nDeposit Groups" as controladdindividualpeopledepositgroups
	control "Remove Individual\nPeople From\nDeposit Groups" as controlremoveindividualpeopledepositgroups

	actordigitalrecordsarchivist --- thingmethodaddindividualpeopledepositgroupsdigitalrecordsarchivist
	thingindividualpeople --- controladdindividualpeopledepositgroups
	thingdepositgroups --- controladdindividualpeopledepositgroups
	thingmethodaddindividualpeopledepositgroupsdigitalrecordsarchivist --> controladdindividualpeopledepositgroups
	thingdepositgroups --- controlremoveindividualpeopledepositgroups
	thingindividualpeople --- controlremoveindividualpeopledepositgroups
	thingmethodaddindividualpeopledepositgroupsdigitalrecordsarchivist --> controlremoveindividualpeopledepositgroups

@enduml