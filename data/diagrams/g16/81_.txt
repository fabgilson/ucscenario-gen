
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Gem" as thinggem #grey
	entity "Test" as thingtest
	entity "Version" as thingversion
	entity "Active Fedora" as thingactivefedora
	entity "Fedora" as thingfedora
	entity "Ddr Batch" as thingddrbatch #grey
	actor "Developer" as actordeveloper
	boundary "Fedora\nInterface" as thingfedorainterface #grey
	boundary "Gem\nInterface" as thinggeminterface #grey
	control "Provided With\nA Version\nOf The\nDdr Batch\nGem Works\nWith Active\nFedora" as controlprovideversion
	control "Provided With\nA Version\nOf The\nGem" as controlprovideversionpasstestfedora
	control "Pass All\nTests On\nFedora" as controlpasstestfedora

	thingddrbatch <.. thinggem
	thingactivefedora <.. thinggem
	thingfedora <.. thingtest
	thinggem *-- thingversion
	actordeveloper --- thingfedorainterface
	actordeveloper --- thinggeminterface
	thingversion --- controlprovideversion
	thingactivefedora --- controlprovideversion
	thingfedorainterface --> controlprovideversion
	thinggem --- controlprovideversion
	thinggem --- controlprovideversionpasstestfedora
	thinggeminterface --> controlprovideversionpasstestfedora
	thingversion --- controlprovideversionpasstestfedora
	controlprovideversionpasstestfedora --> controlpasstestfedora
	thingtest --- controlpasstestfedora
	thingfedora --- controlpasstestfedora
	thingfedorainterface --> controlpasstestfedora

@enduml