
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dul Hydra" as thingdulhydra #grey
	entity "Test" as thingtest
	entity "Active Fedora" as thingactivefedora
	actor "Developer" as actordeveloper
	circle "Version" as thingversion
	boundary "Fedora\nInterface" as thingfedorainterface #grey
	control "Provided With\nA Version\nOf Dul\nHydra Works\nWith Active\nFedora" as controlprovideversion

	thingactivefedora <.. thingdulhydra
	thingdulhydra *-- thingversion
	thingactivefedora *-- thingversion
	actordeveloper --- thingfedorainterface
	thingversion --- controlprovideversion
	thingdulhydra --- controlprovideversion
	thingactivefedora --- controlprovideversion
	thingfedorainterface --> controlprovideversion

@enduml