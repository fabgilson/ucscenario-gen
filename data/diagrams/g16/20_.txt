
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Object" as thingobject
	entity "Embargo" as thingembargo
	actor "Collection Curator" as actorcollectioncurator
	boundary "Simple Way" as thingsimplewayliftembargocollectioncurator
	control "Lift An\nEmbargo For\nAn Object" as controlliftembargo

	thingobject <.. thingembargo
	actorcollectioncurator --- thingsimplewayliftembargocollectioncurator
	thingsimplewayliftembargocollectioncurator --> controlliftembargo
	thingembargo --- controlliftembargo
	thingobject --- controlliftembargo

@enduml