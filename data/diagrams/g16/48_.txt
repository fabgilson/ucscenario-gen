
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Repository Index" as thingrepositoryindex
	entity "Public Interface" as thingpublicinterface
	entity "Access" as thingaccess
	actor "Repository Administrator" as actorrepositoryadministrator
	boundary "Interface\nInterface" as thinginterfaceinterface #grey
	control "Update The\nRepository Index\nDisrupting Access\nTo The\nPublic Interface" as controlupdaterepositoryindexaccess

	thingaccess <.. thingrepositoryindex
	thingpublicinterface <.. thingaccess
	actorrepositoryadministrator --- thinginterfaceinterface
	thingrepositoryindex --- controlupdaterepositoryindexaccess
	thingaccess --- controlupdaterepositoryindexaccess
	thingpublicinterface --- controlupdaterepositoryindexaccess
	thinginterfaceinterface --> controlupdaterepositoryindexaccess

@enduml