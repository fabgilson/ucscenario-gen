
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Summit" as thingsummit
	entity "Agenda" as thingagenda
	actor "Summit Coordinator" as actorsummitcoordinator
	boundary "Summit\nInterface" as thingsummitinterface #grey
	control "List An\nAgenda For\nSummit" as controllistagenda

	thingsummit <.. thingagenda
	actorsummitcoordinator --- thingsummitinterface
	thingagenda --- controllistagenda
	thingsummit --- controllistagenda
	thingsummitinterface --> controllistagenda

@enduml