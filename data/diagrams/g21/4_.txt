
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Training" as thingtraining
	actor "Trainer" as actortrainer
	boundary "Training\nInterface" as thingtraininginterface #grey
	control "Edit Training" as controledittraining

	actortrainer --- thingtraininginterface
	thingtraining --- controledittraining
	thingtraininginterface --> controledittraining

@enduml