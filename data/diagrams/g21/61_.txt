
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Badcamp" as thingBADCamp
	actor "Anonymoususer" as actoranonymoususer
	boundary "Badcamp\nInterface" as thingbadcampinterface #grey
	control "Speak At\nBadcamp" as controlspeakBADCamp
	control "Register" as controlregisterspeakBADCamp

	actoranonymoususer --- thingbadcampinterface
	thingBADCamp --- controlspeakBADCamp
	controlregisterspeakBADCamp --> controlspeakBADCamp
	thingbadcampinterface --> controlspeakBADCamp
	thingbadcampinterface --> controlregisterspeakBADCamp

@enduml