
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "User Profiles" as thinguserprofiles #grey
	actor "Anonymoususer" as actoranonymoususer
	boundary "Profiles\nInterface" as thingprofilesinterface #grey
	control "View A\nList Of\nUser Profiles" as controlviewlist

	actoranonymoususer --- thingprofilesinterface
	thinguserprofiles --- controlviewlist
	thingprofilesinterface --> controlviewlist

@enduml