
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Trainee Information" as thingtraineeinformation #grey
	entity "View" as thingview #grey
	entity "Training Session" as thingtrainingsession
	entity "Admin" as thingadmin #grey
	actor "Trainingcoordinator" as actortrainingcoordinator
	boundary "View\nInterface" as thingviewinterface #grey
	boundary "Session\nInterface" as thingsessioninterface #grey
	boundary "Information\nInterface" as thinginformationinterface #grey
	control "Filter" as controlfiltershowsingletrainingsession
	control "Have An\nView" as controlhaveviewfiltershowsingletrainingsession
	control "Show A\nSingle Training\nSession" as controlshowsingletrainingsession
	control "Have An\nAdmin View\nAll Trainee\nInformation Shown" as controlhaveadminview

	thingadmin <.. thingview
	thingtraineeinformation <.. thingview
	actortrainingcoordinator --- thingviewinterface
	actortrainingcoordinator --- thingsessioninterface
	actortrainingcoordinator --- thinginformationinterface
	controlhaveviewfiltershowsingletrainingsession --> controlfiltershowsingletrainingsession
	thingviewinterface --> controlfiltershowsingletrainingsession
	thingview --- controlhaveviewfiltershowsingletrainingsession
	thingviewinterface --> controlhaveviewfiltershowsingletrainingsession
	controlfiltershowsingletrainingsession --> controlshowsingletrainingsession
	thingtrainingsession --- controlshowsingletrainingsession
	thingsessioninterface --> controlshowsingletrainingsession
	thingtraineeinformation --- controlhaveadminview
	thinginformationinterface --> controlhaveadminview
	thingview --- controlhaveadminview

@enduml