
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Waitlist Training" as thingwaitlisttraining
	actor "Attendee" as actorattendee
	boundary "Training\nInterface" as thingtraininginterface #grey
	control "Added To\nA Waitlist\nTraining" as controladdwaitlisttraining

	actorattendee --- thingtraininginterface
	thingwaitlisttraining --- controladdwaitlisttraining
	thingtraininginterface --> controladdwaitlisttraining

@enduml