
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Article" as thingarticle
	actor "Site Visitor" as actorsitevisitor
	boundary "Article\nInterface" as thingarticleinterface #grey
	control "Have Articles\nInterest" as controlhavearticle
	control "Get" as controlget
	control "Have Articles" as controlhavearticlebeget
	control "Be" as controlbeget

	thingarticle <.. thingarticle
	actorsitevisitor --- thingarticleinterface
	thingarticle --- controlhavearticle
	thingarticleinterface --> controlhavearticle
	controlbeget --> controlget
	thingarticleinterface --> controlget
	thingarticleinterface --> controlhavearticlebeget
	thingarticle --- controlhavearticlebeget
	controlhavearticlebeget --> controlbeget
	thingarticleinterface --> controlbeget

@enduml