
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dataset Type" as thingdatasettype #grey
	entity "App" as thingapp
	entity "Unit Tests" as thingunittests #grey
	actor "App Developer" as actorappdeveloper
	circle "Interface" as thinginterface
	boundary "Type\nInterface" as thingtypeinterface #grey
	control "Write Unit\nTests For\nAn App\nDepends On\nThe Interface\nOf A\nDataset Type" as controlwriteunittests

	thinginterface <.. thingapp
	thingapp <.. thingunittests
	thingdatasettype *-- thinginterface
	actorappdeveloper --- thingtypeinterface
	thingunittests --- controlwriteunittests
	thingapp --- controlwriteunittests
	thinginterface --- controlwriteunittests
	thingdatasettype --- controlwriteunittests
	thingtypeinterface --> controlwriteunittests

@enduml