
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "App" as thingapp
	entity "Dataset Type" as thingdatasettype #grey
	actor "App Developer" as actorappdeveloper
	boundary "App\nInterface" as thingappinterface #grey
	boundary "Type\nInterface" as thingtypeinterface #grey
	control "That Deployed\nAs Part\nOf An\nApp" as controldeploysharedatasettypethatpart
	control "Share A\nDataset Type" as controlsharedatasettype

	actorappdeveloper --- thingappinterface
	actorappdeveloper --- thingtypeinterface
	thingapp --- controldeploysharedatasettypethatpart
	thingappinterface --> controldeploysharedatasettypethatpart
	controldeploysharedatasettypethatpart --> controlsharedatasettype
	thingdatasettype --- controlsharedatasettype
	thingtypeinterface --> controlsharedatasettype

@enduml