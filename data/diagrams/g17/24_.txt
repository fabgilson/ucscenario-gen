
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Pipeline" as thingpipeline
	entity "Dataset Instance" as thingdatasetinstance
	actor "Hydrator User" as actorhydratoruser
	boundary "Pipeline\nInterface" as thingpipelineinterface #grey
	boundary "Instance\nInterface" as thinginstanceinterface #grey
	control "Create A\nPipeline" as controlcreatepipelinewriteexistingdatasetinstance
	control "Write An\nExisting Dataset\nInstance" as controlwriteexistingdatasetinstance
	control "Create A\nPipeline Reads" as controlcreatepipeline

	actorhydratoruser --- thingpipelineinterface
	actorhydratoruser --- thinginstanceinterface
	thingpipelineinterface --> controlcreatepipelinewriteexistingdatasetinstance
	thingpipeline --- controlcreatepipelinewriteexistingdatasetinstance
	controlcreatepipelinewriteexistingdatasetinstance --> controlwriteexistingdatasetinstance
	thingdatasetinstance --- controlwriteexistingdatasetinstance
	thinginstanceinterface --> controlwriteexistingdatasetinstance
	thingpipeline --- controlcreatepipeline
	thingpipelineinterface --> controlcreatepipeline

@enduml