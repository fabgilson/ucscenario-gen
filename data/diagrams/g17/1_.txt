
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dataset Type" as thingdatasettype #grey
	entity "App" as thingapp
	actor "App Developer" as actorappdeveloper
	circle "New Version" as thingnewversion
	boundary "App\nInterface" as thingappinterface #grey
	control "Deploy A\nNew Version\nOf A\nDataset Type\nAs Part\nDeploying A\nNew Version\nOf The\nApp Includes" as controldeploynewversionpart

	thingapp <.. thingdatasettype
	thingnewversion <.. thingapp
	thingapp *-- thingnewversion
	thingdatasettype *-- thingnewversion
	actorappdeveloper --- thingappinterface
	thingdatasettype --- controldeploynewversionpart
	thingapp --- controldeploynewversionpart
	thingappinterface --> controldeploynewversionpart
	thingnewversion --- controldeploynewversionpart

@enduml