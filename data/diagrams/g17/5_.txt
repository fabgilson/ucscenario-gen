
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Multiple Application" as thingmultipleapplication
	entity "Dataset Type" as thingdatasettype #grey
	entity "Code" as thingcode
	entity "Artifact" as thingartifact
	actor "App Developer" as actorappdeveloper
	boundary "Artifact\nInterface" as thingartifactinterface #grey
	control "Share A\nDataset Type\nAcross Multiple\nApplications Include\nThe Dataset\nType's Code\nIn Artifacts" as controlsharedatasettypemultipleapplication

	thingcode <.. thingmultipleapplication
	thingmultipleapplication <.. thingdatasettype
	thingartifact <.. thingcode
	thingdatasettype <.. thingcode
	actorappdeveloper --- thingartifactinterface
	thingdatasettype --- controlsharedatasettypemultipleapplication
	thingmultipleapplication --- controlsharedatasettypemultipleapplication
	thingcode --- controlsharedatasettypemultipleapplication
	thingartifact --- controlsharedatasettypemultipleapplication
	thingartifactinterface --> controlsharedatasettypemultipleapplication

@enduml