
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Option" as thingoption
	entity "Patron" as thingpatron
	entity "Relationship Groups" as thingrelationshipgroups
	actor "Db Administrator" as actordbadministrator
	circle "Support" as thingsupport
	boundary "Option\nInterface" as thingoptioninterface #grey
	control "Manage Customizable\nRelationship Groups\nIn Support\nOf Patron\nOption" as controlmanagecustomizablerelationshipgroupssupportoption

	thingoption <.. thingpatron
	thingsupport <.. thingrelationshipgroups
	thingpatron *-- thingsupport
	actordbadministrator --- thingoptioninterface
	thingrelationshipgroups --- controlmanagecustomizablerelationshipgroupssupportoption
	thingsupport --- controlmanagecustomizablerelationshipgroupssupportoption
	thingpatron --- controlmanagecustomizablerelationshipgroupssupportoption
	thingoption --- controlmanagecustomizablerelationshipgroupssupportoption
	thingoptioninterface --> controlmanagecustomizablerelationshipgroupssupportoption

@enduml