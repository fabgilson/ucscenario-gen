
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Item" as thingitem
	entity "Revisor" as thingrevisor
	entity "Repository" as thingrepository
	actor "Library ,\nStaff Member" as actorlibrarystaffmember
	boundary "Revisor\nInterface" as thingrevisorinterface #grey
	boundary "Repository\nInterface" as thingrepositoryinterface #grey
	control "Determine" as controldetermineuploaditemrepository
	control "Trace The\nList Of\nRevisors" as controltracelistdetermineuploaditemrepository
	control "Uploaded An\nItem To\nThe Repository" as controluploaditemrepository

	thingrepository <.. thingitem
	actorlibrarystaffmember --- thingrevisorinterface
	actorlibrarystaffmember --- thingrepositoryinterface
	controltracelistdetermineuploaditemrepository --> controldetermineuploaditemrepository
	thingrevisorinterface --> controldetermineuploaditemrepository
	thingrevisor --- controltracelistdetermineuploaditemrepository
	thingrevisorinterface --> controltracelistdetermineuploaditemrepository
	controldetermineuploaditemrepository --> controluploaditemrepository
	thingitem --- controluploaditemrepository
	thingrepository --- controluploaditemrepository
	thingrepositoryinterface --> controluploaditemrepository

@enduml