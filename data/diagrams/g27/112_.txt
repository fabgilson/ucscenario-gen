
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Record" as thingrecord
	entity "Mechanism" as thingmechanism
	entity "Item" as thingitem
	actor "Datum Curator" as actordatacurator
	boundary "Mechanism\nInterface" as thingmechanisminterface #grey
	control "Allow For\nRecords For\nItems Not\nDo Need" as controlallowrecord
	control "Stored" as controlstore
	control "Have A\nMechanism" as controlhavemechanismallowrecord

	thingitem <.. thingrecord
	actordatacurator --- thingmechanisminterface
	thingrecord --- controlallowrecord
	thingitem --- controlallowrecord
	controlhavemechanismallowrecord --> controlallowrecord
	thingmechanisminterface --> controlallowrecord
	controlallowrecord --> controlstore
	thingmechanisminterface --> controlstore
	thingmechanism --- controlhavemechanismallowrecord
	thingmechanisminterface --> controlhavemechanismallowrecord

@enduml