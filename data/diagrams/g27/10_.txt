
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Repository" as thingrepository
	entity "Publication" as thingpublication
	entity "Access Point" as thingaccesspoint
	actor "Faculty Member" as actorfacultymember
	boundary "Repository\nInterface" as thingrepositoryinterface #grey
	boundary "Point\nInterface" as thingpointinterface #grey
	control "Collect Those\nWithout" as controlcollectthosewithout
	control "Collect Those\nWithin The\nRepository" as controlcollectthose
	control "Collect Publications\nInto A\nOnline, Single\nAccess Point" as controlcollectpublicationonlinesingleaccesspoint

	thingaccesspoint <.. thingpublication
	actorfacultymember --- thingrepositoryinterface
	actorfacultymember --- thingpointinterface
	thingrepository --- controlcollectthose
	thingrepositoryinterface --> controlcollectthose
	thingpublication --- controlcollectpublicationonlinesingleaccesspoint
	thingaccesspoint --- controlcollectpublicationonlinesingleaccesspoint
	thingpointinterface --> controlcollectpublicationonlinesingleaccesspoint

@enduml