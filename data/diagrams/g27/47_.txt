
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Faculty" as thingfaculty #grey
	entity "Research Material" as thingresearchmaterial
	entity "Evidence ,\nBased Programming" as thingevidencebasedprogramming
	entity "Strong Research" as thingstrongerresearch
	entity "Particular Topic" as thingparticulartopic
	entity "Community Policy" as thingcommunitypolicy #grey
	entity "Business Practice" as thingbusinesspractice #grey
	entity "Grant Proposal" as thinggrantproposal #grey
	entity "Educational" as thingeducational
	entity "Cornell" as thingcornell #grey
	actor "Externaluser" as actorexternaluser
	boundary "Educational\nInterface" as thingeducationalinterface #grey
	boundary "Practice\nInterface" as thingpracticeinterface #grey
	boundary "Topic\nInterface" as thingtopicinterface #grey
	boundary "Policy\nInterface" as thingpolicyinterface #grey
	boundary "Programming\nInterface" as thingprogramminginterface #grey
	boundary "Research\nInterface" as thingresearchinterface #grey
	boundary "Proposal\nInterface" as thingproposalinterface #grey
	control "Find" as controlfind
	control "Access Research\nMaterials From\nFaculty To\nA Particular\nTopic" as controlaccessresearchmaterialfaculty #grey
	control "Access Educational" as controlaccesseducational
	control "Develop Business\nPractice" as controldevelopbusinesspractice
	control "Access Research\nMaterials From\nCornell Faculty\nTo A\nParticular Topic" as controlaccessresearchmaterialcornellfaculty
	control "Develop Community\nPolicy" as controldevelopcommunitypolicy
	control "Which Use" as controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy
	control "Develop Evidence,\nBased Programming" as controldevelopevidencebasedprogramming
	control "Develop Stronger\nResearch" as controldevelopstrongerresearch
	control "Develop Grant\nProposal" as controldevelopgrantproposal

	thingcornell <.. thingfaculty
	thingparticulartopic <.. thingfaculty
	thingfaculty <.. thingresearchmaterial
	actorexternaluser --- thingeducationalinterface
	actorexternaluser --- thingpracticeinterface
	actorexternaluser --- thingtopicinterface
	actorexternaluser --- thingpolicyinterface
	actorexternaluser --- thingprogramminginterface
	actorexternaluser --- thingresearchinterface
	actorexternaluser --- thingproposalinterface
	controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy --> controlaccessresearchmaterialfaculty
	thingfaculty --- controlaccessresearchmaterialfaculty
	thingresearchmaterial --- controlaccessresearchmaterialfaculty
	thingparticulartopic --- controlaccessresearchmaterialfaculty
	thingeducational --- controlaccesseducational
	thingeducationalinterface --> controlaccesseducational
	thingbusinesspractice --- controldevelopbusinesspractice
	thingpracticeinterface --> controldevelopbusinesspractice
	controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy --> controldevelopbusinesspractice
	thingresearchmaterial --- controlaccessresearchmaterialcornellfaculty
	thingparticulartopic --- controlaccessresearchmaterialcornellfaculty
	thingtopicinterface --> controlaccessresearchmaterialcornellfaculty
	thingfaculty --- controlaccessresearchmaterialcornellfaculty
	controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy --> controlaccessresearchmaterialcornellfaculty
	thingcommunitypolicy --- controldevelopcommunitypolicy
	controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy --> controldevelopcommunitypolicy
	thingpolicyinterface --> controldevelopcommunitypolicy
	thingpolicyinterface --> controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy
	thingpracticeinterface --> controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy
	thingprogramminginterface --> controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy
	thingresearchinterface --> controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy
	thingproposalinterface --> controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy
	thingevidencebasedprogramming --- controldevelopevidencebasedprogramming
	thingprogramminginterface --> controldevelopevidencebasedprogramming
	controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy --> controldevelopevidencebasedprogramming
	thingstrongerresearch --- controldevelopstrongerresearch
	thingresearchinterface --> controldevelopstrongerresearch
	controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy --> controldevelopstrongerresearch
	thinggrantproposal --- controldevelopgrantproposal
	thingproposalinterface --> controldevelopgrantproposal
	controluseaccessresearchmaterialfacultywhichmoinsPRONmoinsdevelopdevelopdevelopdevelopcommunitypolicy --> controldevelopgrantproposal

@enduml