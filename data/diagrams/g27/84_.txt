
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Authorized Function" as thingauthorizedfunction
	entity "Access" as thingaccess
	entity "Personal Account" as thingpersonalaccount
	actor "Db Administrator" as actordbadministrator
	boundary "Function\nInterface" as thingfunctioninterface #grey
	control "Login To\nPersonal Account\nWith Access\nTo Authorized\nFunctions" as controlloginpersonalaccountaccess

	thingauthorizedfunction <.. thingaccess
	thingaccess <.. thingpersonalaccount
	actordbadministrator --- thingfunctioninterface
	thingpersonalaccount --- controlloginpersonalaccountaccess
	thingaccess --- controlloginpersonalaccountaccess
	thingauthorizedfunction --- controlloginpersonalaccountaccess
	thingfunctioninterface --> controlloginpersonalaccountaccess

@enduml