
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Ir" as thingIR
	entity "Asset" as thingasset
	actor "Asset Manager" as actorassetmanager
	circle "Disposition" as thingdisposition
	boundary "Ir\nInterface" as thingirinterface #grey
	control "The Ir\nCease" as controlceaseIR
	control "Assured" as controlassure

	thingasset *-- thingdisposition
	actorassetmanager --- thingirinterface
	thingIR --- controlceaseIR
	thingirinterface --> controlceaseIR

@enduml