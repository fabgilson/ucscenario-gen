
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Agreement" as thingagreement
	entity "Project" as thingproject
	entity "Party Data" as thingpartydata
	actor "Researcher" as actorresearcher
	boundary "Project\nInterface" as thingprojectinterface #grey
	control "Store Agreements\nTo Third\nParty Data\nIn A\nProject" as controlstoreagreement

	thingpartydata <.. thingagreement
	thingproject <.. thingpartydata
	actorresearcher --- thingprojectinterface
	thingagreement --- controlstoreagreement
	thingpartydata --- controlstoreagreement
	thingproject --- controlstoreagreement
	thingprojectinterface --> controlstoreagreement

@enduml