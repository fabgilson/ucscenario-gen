
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Section" as thingsection
	actor "Faculty ,\nData Steward" as actorfacultydatasteward
	boundary "Section\nInterface" as thingsectioninterface #grey
	control "See The\nSections Costing" as controlseesection

	actorfacultydatasteward --- thingsectioninterface
	thingsection --- controlseesection
	thingsectioninterface --> controlseesection

@enduml