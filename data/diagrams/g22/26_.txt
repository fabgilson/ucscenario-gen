
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dmp" as thingDMP
	actor "Stakeholder" as actorstakeholder
	boundary "Dmp\nInterface" as thingdmpinterface #grey
	control "Know Responsible\nFor The\nDmp" as controlknowDMP

	actorstakeholder --- thingdmpinterface
	thingDMP --- controlknowDMP
	thingdmpinterface --> controlknowDMP

@enduml