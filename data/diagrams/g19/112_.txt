
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Game" as thinggame
	entity "User" as thinguser
	entity "Carer" as thingcarer
	actor "Informalcaregiver" as actorInformalCaregiver
	boundary "User\nInterface" as thinguserinterface #grey
	control "Have A\nGame That\nThe Carer\nTogether Play\nWith The\nUser" as controlhavegame

	thingcarer <.. thinggame
	thinguser <.. thinggame
	thinguser <.. thingcarer
	actorInformalCaregiver --- thinguserinterface
	thinggame --- controlhavegame
	thingcarer --- controlhavegame
	thinguser --- controlhavegame
	thinguserinterface --> controlhavegame

@enduml