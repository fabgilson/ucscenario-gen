
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Market Place" as thingmarketplace #grey
	entity "App" as thingapp
	actor "Developer" as actordeveloper
	boundary "Place\nInterface" as thingplaceinterface #grey
	control "Create Apps\nWithin The\nMarket Place" as controlcreateappmarketplace
	control "Maintain Apps\nWithin The\nMarket Place" as controlmaintainappmarketplace #grey

	thingmarketplace <.. thingapp
	actordeveloper --- thingplaceinterface
	thingapp --- controlcreateappmarketplace
	thingmarketplace --- controlcreateappmarketplace
	thingplaceinterface --> controlcreateappmarketplace
	thingmarketplace --- controlmaintainappmarketplace
	thingapp --- controlmaintainappmarketplace

@enduml