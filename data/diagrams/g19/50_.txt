
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Medicine Reminder" as thingmedicinereminder #grey
	entity "Alfred" as thingALFRED
	actor "Olderperson" as actorOlderPerson
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	control "Receive A\nMedicine Reminder\nFrom Alfred" as controlreceivemedicinereminder

	thingALFRED <.. thingmedicinereminder
	actorOlderPerson --- thingalfredinterface
	thingmedicinereminder --- controlreceivemedicinereminder
	thingALFRED --- controlreceivemedicinereminder
	thingalfredinterface --> controlreceivemedicinereminder

@enduml