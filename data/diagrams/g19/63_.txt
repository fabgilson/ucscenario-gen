
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Speech" as thingspeech
	entity "Alfred" as thingALFRED
	entity "Meeting" as thingmeeting
	actor "Olderperson" as actorOlderPerson
	boundary "Speech\nInterface" as thingspeechinterface #grey
	control "Insert Meetings\nInto Alfred\nUsing Speech" as controlinsertmeetingALFREDspeech

	thingspeech <.. thingALFRED
	thingALFRED <.. thingmeeting
	actorOlderPerson --- thingspeechinterface
	thingmeeting --- controlinsertmeetingALFREDspeech
	thingALFRED --- controlinsertmeetingALFREDspeech
	thingspeech --- controlinsertmeetingALFREDspeech
	thingspeechinterface --> controlinsertmeetingALFREDspeech

@enduml