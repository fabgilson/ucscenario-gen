
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Service" as thingservice
	entity "Sale" as thingsale
	entity "Trouble" as thingtrouble
	entity "Alfred" as thingALFRED
	entity "Clear Instruction" as thingclearinstruction
	actor "Olderperson" as actorOlderPerson
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	boundary "Service\nInterface" as thingserviceinterface #grey
	boundary "Sale\nInterface" as thingsaleinterface #grey
	control "Clear Instructions\nGuide Have\nTrouble Using\nAlfred" as controlhaveclearinstructiontroubleALFRED
	control "Have Service" as controlhaveservice
	control "Have Clear\nInstructions After\nSales" as controlhaveclearinstructionsale

	thingALFRED <.. thingtrouble
	thingsale <.. thingclearinstruction
	thingtrouble <.. thingclearinstruction
	actorOlderPerson --- thingalfredinterface
	actorOlderPerson --- thingserviceinterface
	actorOlderPerson --- thingsaleinterface
	thingtrouble --- controlhaveclearinstructiontroubleALFRED
	thingALFRED --- controlhaveclearinstructiontroubleALFRED
	thingalfredinterface --> controlhaveclearinstructiontroubleALFRED
	thingclearinstruction --- controlhaveclearinstructiontroubleALFRED
	thingservice --- controlhaveservice
	thingserviceinterface --> controlhaveservice
	thingclearinstruction --- controlhaveclearinstructionsale
	thingsale --- controlhaveclearinstructionsale
	thingsaleinterface --> controlhaveclearinstructionsale

@enduml