
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Touristic Walk" as thingtouristicwalk
	entity "Alfred" as thingALFRED
	entity "Indication" as thingindication
	entity "Explanation" as thingexplanation
	entity "Cultural" as thingcultural
	entity "Historical Sight" as thinghistoricalsight
	actor "Olderperson" as actorOlderPerson
	boundary "Walk\nInterface" as thingwalkinterface #grey
	boundary "Cultural\nInterface" as thingculturalinterface #grey
	boundary "Sight\nInterface" as thingsightinterface #grey
	control "Use Alfred\nFor Touristic\nWalks" as controluseALFREDtouristicwalk
	control "Use Alfred\nFor Indication\nWith Explanations\nOn Cultural" as controluseALFREDindicationcultural
	control "Use Alfred\nFor Indication\nWith Explanations\nOn Historical\nSights" as controluseALFREDindication

	thingtouristicwalk <.. thingALFRED
	thingindication <.. thingALFRED
	thingexplanation <.. thingindication
	thinghistoricalsight <.. thingexplanation
	thingcultural <.. thingexplanation
	actorOlderPerson --- thingwalkinterface
	actorOlderPerson --- thingculturalinterface
	actorOlderPerson --- thingsightinterface
	thingALFRED --- controluseALFREDtouristicwalk
	thingtouristicwalk --- controluseALFREDtouristicwalk
	thingwalkinterface --> controluseALFREDtouristicwalk
	thingcultural --- controluseALFREDindicationcultural
	thingculturalinterface --> controluseALFREDindicationcultural
	thingALFRED --- controluseALFREDindicationcultural
	thingindication --- controluseALFREDindicationcultural
	thingexplanation --- controluseALFREDindicationcultural
	thingindication --- controluseALFREDindication
	thingexplanation --- controluseALFREDindication
	thinghistoricalsight --- controluseALFREDindication
	thingsightinterface --> controluseALFREDindication
	thingALFRED --- controluseALFREDindication

@enduml