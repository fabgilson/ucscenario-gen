
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Alfred" as thingALFRED
	entity "Thing" as thingthing
	entity "Person" as thingperson
	actor "Socialcaregiver" as actorSocialCaregiver
	boundary "Person\nInterface" as thingpersoninterface #grey
	boundary "Thing\nInterface" as thingthinginterface #grey
	control "Bring To\nThe Person" as controlbringperson
	control "Need" as controlneedremindALFREDthingcarebringperson
	control "Alfred Remind\nOf The\nThings" as controlremindALFREDthing
	control "Care" as controlcarebringperson

	thingthing <.. thingALFRED
	actorSocialCaregiver --- thingpersoninterface
	actorSocialCaregiver --- thingthinginterface
	thingperson --- controlbringperson
	controlcarebringperson --> controlbringperson
	thingpersoninterface --> controlbringperson
	thingthinginterface --> controlneedremindALFREDthingcarebringperson
	controlneedremindALFREDthingcarebringperson --> controlremindALFREDthing
	thingthing --- controlremindALFREDthing
	thingthinginterface --> controlremindALFREDthing
	thingALFRED --- controlremindALFREDthing
	controlneedremindALFREDthingcarebringperson --> controlcarebringperson

@enduml