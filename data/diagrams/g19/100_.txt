
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Alfred" as thingALFRED
	entity "Body Posture" as thingbodyposture
	actor "Olderperson" as actorOlderPerson
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	control "Keep A\nGood Body\nPosture" as controlkeepgoodbodyposture
	control "Alfred Remind" as controlremindALFREDkeepgoodbodyposture
	control "Have Alfred" as controlhaveALFREDremindALFREDkeepgoodbodyposture

	actorOlderPerson --- thingalfredinterface
	thingbodyposture --- controlkeepgoodbodyposture
	controlremindALFREDkeepgoodbodyposture --> controlkeepgoodbodyposture
	thingalfredinterface --> controlkeepgoodbodyposture
	controlhaveALFREDremindALFREDkeepgoodbodyposture --> controlremindALFREDkeepgoodbodyposture
	thingalfredinterface --> controlremindALFREDkeepgoodbodyposture
	thingALFRED --- controlremindALFREDkeepgoodbodyposture
	thingALFRED --- controlhaveALFREDremindALFREDkeepgoodbodyposture
	thingalfredinterface --> controlhaveALFREDremindALFREDkeepgoodbodyposture

@enduml