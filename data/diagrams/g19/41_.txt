
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Light" as thinglight
	entity "Alfred" as thingALFRED
	actor "Olderperson" as actorOlderPerson
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	boundary "Light\nInterface" as thinglightinterface #grey
	control "Use Alfred" as controluseALFREDturnturnoff
	control "Turn On" as controlturn
	control "Turn Off\nThe Lights" as controlturnoff

	actorOlderPerson --- thingalfredinterface
	actorOlderPerson --- thinglightinterface
	thingALFRED --- controluseALFREDturnturnoff
	thingalfredinterface --> controluseALFREDturnturnoff
	thingalfredinterface --> controlturn
	controluseALFREDturnturnoff --> controlturn
	controluseALFREDturnturnoff --> controlturnoff
	thinglight --- controlturnoff
	thinglightinterface --> controlturnoff

@enduml