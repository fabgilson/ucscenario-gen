
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Mental Exercise" as thingmentalexercise
	entity "Leg" as thingleg
	entity "Arm" as thingarm
	entity "Combination" as thingcombination #grey
	entity "Motion Exercises" as thingmotionexercises #grey
	entity "Physical" as thingphysical
	entity "Exercise" as thingexercise #grey
	actor "Olderperson" as actorOlderPerson
	circle "Combination" as thingsuchcombination
	boundary "Physical\nInterface" as thingphysicalinterface #grey
	boundary "Arm\nInterface" as thingarminterface #grey
	boundary "Leg\nInterface" as thingleginterface #grey
	control "Games Require\nThe Combination\nOf Physical" as controlgamecombinationphysical
	control "Games Require\nThe Such\nCombination Of\nMental Exercise\nOpposing Motion\nExercises Of\nThe Arms" as controlgamesuchcombination
	control "Games Require\nThe Such\nCombination Of\nMental Exercise\nOpposing Exercises\nOf Leg" as controlgamesuchcombinationleg

	thingmotionexercises <.. thingmentalexercise
	thingexercise <.. thingmentalexercise
	thingphysical <.. thingcombination
	thingarm <.. thingmotionexercises
	thingleg <.. thingexercise
	thingmentalexercise *-- thingsuchcombination
	thingmotionexercises *-- thingsuchcombination
	thingexercise *-- thingsuchcombination
	actorOlderPerson --- thingphysicalinterface
	actorOlderPerson --- thingarminterface
	actorOlderPerson --- thingleginterface
	thingcombination --- controlgamecombinationphysical
	thingphysical --- controlgamecombinationphysical
	thingphysicalinterface --> controlgamecombinationphysical
	thingsuchcombination --- controlgamesuchcombination
	thingmentalexercise --- controlgamesuchcombination
	thingmotionexercises --- controlgamesuchcombination
	thingarm --- controlgamesuchcombination
	thingarminterface --> controlgamesuchcombination
	thingexercise --- controlgamesuchcombinationleg
	thingleg --- controlgamesuchcombinationleg
	thingleginterface --> controlgamesuchcombinationleg
	thingmentalexercise --- controlgamesuchcombinationleg
	thingsuchcombination --- controlgamesuchcombinationleg

@enduml