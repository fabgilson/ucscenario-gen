
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Predicate Link" as thingpredicatedlink
	entity "File" as thingfile
	actor "User" as actoruser
	boundary "File\nInterface" as thingfileinterface #grey
	control "Make Predicated\nLinks Between\nFiles" as controlmakepredicatedlink

	thingfile <.. thingpredicatedlink
	actoruser --- thingfileinterface
	thingpredicatedlink --- controlmakepredicatedlink
	thingfile --- controlmakepredicatedlink
	thingfileinterface --> controlmakepredicatedlink

@enduml