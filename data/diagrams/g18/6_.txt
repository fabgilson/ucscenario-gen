
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Web Interface\nGraphics" as thingwebinterfacegraphics #grey
	entity "Colour" as thingcolour
	actor "Lab Administrator" as actorlabadministrator
	boundary "Colour\nInterface" as thingcolourinterface #grey
	boundary "Graphics\nInterface" as thinggraphicsinterface #grey
	control "Theme Colour" as controlthemecolour
	control "Theme The\nWeb Interface\nGraphics" as controlthemewebinterfacegraphics

	actorlabadministrator --- thingcolourinterface
	actorlabadministrator --- thinggraphicsinterface
	thingcolour --- controlthemecolour
	thingcolourinterface --> controlthemecolour
	thingwebinterfacegraphics --- controlthemewebinterfacegraphics
	thinggraphicsinterface --> controlthemewebinterfacegraphics

@enduml