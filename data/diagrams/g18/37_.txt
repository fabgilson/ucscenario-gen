
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Metadata" as thingsamemetadata
	entity "Experimental Run" as thingexperimentalrun
	entity "File" as thingfile
	actor "User" as actoruser
	boundary "Run\nInterface" as thingruninterface #grey
	control "Capture The\nSame Metadata\nFor Each\nFile In\nAn Experimental\nRun" as controlcapturesamemetadata

	thingfile <.. thingsamemetadata
	thingexperimentalrun <.. thingfile
	actoruser --- thingruninterface
	thingsamemetadata --- controlcapturesamemetadata
	thingfile --- controlcapturesamemetadata
	thingexperimentalrun --- controlcapturesamemetadata
	thingruninterface --> controlcapturesamemetadata

@enduml