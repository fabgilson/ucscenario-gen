
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Type" as thingtype
	entity "File" as thingfile
	entity "Search Results" as thingsearchresults #grey
	actor "User" as actoruser
	boundary "Results\nInterface" as thingresultsinterface #grey
	boundary "Type\nInterface" as thingtypeinterface #grey
	control "Get From\nSearch Results" as controlgetfilterfiletypesearchresults
	control "Filter The\nFiles On\nType" as controlfilterfiletype

	thingtype <.. thingfile
	actoruser --- thingresultsinterface
	actoruser --- thingtypeinterface
	thingsearchresults --- controlgetfilterfiletypesearchresults
	thingresultsinterface --> controlgetfilterfiletypesearchresults
	controlgetfilterfiletypesearchresults --> controlfilterfiletype
	thingfile --- controlfilterfiletype
	thingtype --- controlfilterfiletype
	thingtypeinterface --> controlfilterfiletype

@enduml