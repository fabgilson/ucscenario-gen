
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Meta Data" as thingmetadata
	actor "Researcher" as actorresearcher
	boundary "Physiology Images" as thingphysiologyimagesresearcher
	control "Annotated With\nStandard Meta\nData" as controlannotatephysiologyimagesresearcherstandardmetadata
	control "Have" as controlhaveannotatephysiologyimagesresearcherstandardmetadata

	actorresearcher --- thingphysiologyimagesresearcher
	thingphysiologyimagesresearcher --> controlannotatephysiologyimagesresearcherstandardmetadata
	thingmetadata --- controlannotatephysiologyimagesresearcherstandardmetadata
	controlhaveannotatephysiologyimagesresearcherstandardmetadata --> controlannotatephysiologyimagesresearcherstandardmetadata
	thingphysiologyimagesresearcher --> controlhaveannotatephysiologyimagesresearcherstandardmetadata

@enduml