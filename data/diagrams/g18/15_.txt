
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "File" as thingfile
	entity "Web Browser" as thingwebbrowser #grey
	actor "Researcher" as actorresearcher
	boundary "Experiment" as thingexperimentusewebbrowserresearcher #grey
	control "Download Files\nTo" as controldownloadfile
	control "Use Web\nBrowser" as controlusewebbrowser

	actorresearcher --- thingexperimentusewebbrowserresearcher
	thingfile --- controldownloadfile
	thingexperimentusewebbrowserresearcher --> controldownloadfile
	thingexperimentusewebbrowserresearcher --> controlusewebbrowser
	thingwebbrowser --- controlusewebbrowser
	controldownloadfile --> controlusewebbrowser

@enduml