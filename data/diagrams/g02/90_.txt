
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Submission Dashboard" as thingsubmissiondashboard #grey
	entity "Status Labels" as thingstatuslabels
	actor "Fab User" as actorfabsuser
	boundary "Dashboard\nInterface" as thingdashboardinterface #grey
	control "See Correct\nStatus Labels\nOn The\nSubmission Dashboard" as controlseecorrectstatuslabels

	thingsubmissiondashboard <.. thingstatuslabels
	actorfabsuser --- thingdashboardinterface
	thingstatuslabels --- controlseecorrectstatuslabels
	thingsubmissiondashboard --- controlseecorrectstatuslabels
	thingdashboardinterface --> controlseecorrectstatuslabels

@enduml