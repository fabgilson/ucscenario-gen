
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Fabs Go\nLive" as thingfabsgolive #grey
	entity "Assistance Data" as thingassistancedata
	actor "Agency User" as actoragencyuser
	boundary "Live\nInterface" as thingliveinterface #grey
	control "Provided With\nAll Financial,\nHistorical Assistance\nData For\nFabs Go\nLive" as controlprovidefinancialhistoricalassistancedata

	thingfabsgolive <.. thingassistancedata
	actoragencyuser --- thingliveinterface
	thingassistancedata --- controlprovidefinancialhistoricalassistancedata
	thingfabsgolive --- controlprovidefinancialhistoricalassistancedata
	thingliveinterface --> controlprovidefinancialhistoricalassistancedata

@enduml