
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Federalactionobligation" as thingfederalactionobligation
	entity "Atom Feed" as thingatomfeed #grey
	actor "Agency User" as actoragencyuser
	boundary "Feed\nInterface" as thingfeedinterface #grey
	control "Map The\nFederalactionobligation To\nThe Atom\nFeed" as controlmapfederalactionobligationatomfeed

	thingatomfeed <.. thingfederalactionobligation
	actoragencyuser --- thingfeedinterface
	thingfederalactionobligation --- controlmapfederalactionobligationatomfeed
	thingatomfeed --- controlmapfederalactionobligationatomfeed
	thingfeedinterface --> controlmapfederalactionobligationatomfeed

@enduml