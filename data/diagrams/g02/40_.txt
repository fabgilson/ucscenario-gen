
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Loan Record" as thingloanrecord
	entity "Zero" as thingzero
	actor "Agency User" as actoragencyuser
	boundary "Validation Fabs\nRule" as thingvalidationfabsrulesagencyuser
	control "Blank Accept\nZero For\nLoan Records" as controlacceptvalidationfabsrulesagencyuserzeroloanrecord

	actoragencyuser --- thingvalidationfabsrulesagencyuser
	thingvalidationfabsrulesagencyuser --> controlacceptvalidationfabsrulesagencyuserzeroloanrecord
	thingzero --- controlacceptvalidationfabsrulesagencyuserzeroloanrecord
	thingloanrecord --- controlacceptvalidationfabsrulesagencyuserzeroloanrecord

@enduml