
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Historical ,\nExtracted Datum" as thinghistoricalextracteddatum
	entity "Fpds Feed\nDatum" as thingfpdsfeeddatum #grey
	actor "Developer" as actordeveloper
	boundary "Historical Loader" as thinghistoricalloaderincludefpdsfeeddatumdeveloper #grey
	boundary "Historical Fpds\nData Loader" as thinghistoricalfpdsdataloaderincludehistoricalextracteddatumdeveloper
	control "Include Historical,\nExtracted Data" as controlincludehistoricalextracteddatum
	control "Include Fpds\nFeed Datum" as controlincludefpdsfeeddatum

	actordeveloper --- thinghistoricalloaderincludefpdsfeeddatumdeveloper
	actordeveloper --- thinghistoricalfpdsdataloaderincludehistoricalextracteddatumdeveloper
	thinghistoricalextracteddatum --- controlincludehistoricalextracteddatum
	thinghistoricalfpdsdataloaderincludehistoricalextracteddatumdeveloper --> controlincludehistoricalextracteddatum
	thingfpdsfeeddatum --- controlincludefpdsfeeddatum
	thinghistoricalloaderincludefpdsfeeddatumdeveloper --> controlincludefpdsfeeddatum

@enduml