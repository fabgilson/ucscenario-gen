
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	actor "Developer" as actordeveloper
	boundary "Domain Models" as thingdomainmodelsdeveloper
	control "Indexed" as controlindexdomainmodelsdeveloper

	actordeveloper --- thingdomainmodelsdeveloper
	thingdomainmodelsdeveloper --> controlindexdomainmodelsdeveloper

@enduml