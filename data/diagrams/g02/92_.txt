
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dabs" as thingDABS
	entity "Fab Page" as thingfabspage
	actor "Agency User" as actoragencyuser
	boundary "Landing Page" as thinglandingpageagencyuser
	boundary "Page" as thingpageagencyuser #grey
	control "Navigate To\nFabs Pages" as controlnavigatelandingpageagencyuserfabspage
	control "Navigate To\nDabs" as controlnavigatepageagencyuserDABS

	actoragencyuser --- thinglandingpageagencyuser
	actoragencyuser --- thingpageagencyuser
	thinglandingpageagencyuser --> controlnavigatelandingpageagencyuserfabspage
	thingfabspage --- controlnavigatelandingpageagencyuserfabspage
	thingpageagencyuser --> controlnavigatepageagencyuserDABS
	thingDABS --- controlnavigatepageagencyuserDABS

@enduml