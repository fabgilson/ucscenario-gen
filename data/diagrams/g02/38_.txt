
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Datum" as thingdatum
	entity "Sam" as thingSAM
	actor "Agency User" as actoragencyuser

	thingSAM <.. thingdatum

@enduml