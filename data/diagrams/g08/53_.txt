
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Datum" as thingdatum
	entity "Exist Wizard" as thingexistingwizard
	actor "Developer" as actordeveloper
	circle "Specific Type" as thingspecifictype
	boundary "Datum\nInterface" as thingdatuminterface #grey
	control "Customize An\nExisting Wizard\nFor Specific\nType Of\nData" as controlcustomizeexistingwizardspecifictype

	thingspecifictype <.. thingexistingwizard
	thingdatum *-- thingspecifictype
	actordeveloper --- thingdatuminterface
	thingexistingwizard --- controlcustomizeexistingwizardspecifictype
	thingspecifictype --- controlcustomizeexistingwizardspecifictype
	thingdatum --- controlcustomizeexistingwizardspecifictype
	thingdatuminterface --> controlcustomizeexistingwizardspecifictype

@enduml