
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Spending Dataset" as thingspendingdataset #grey
	entity "Inflation" as thinginflation
	entity "Reference Data" as thingreferencedata #grey
	actor "Researchergovernment Publisher" as actorresearchergovernmentpublisher
	boundary "Dataset\nInterface" as thingdatasetinterface #grey
	control "Add Reference\nData On\nInflation To\nSpending Dataset" as controladdreferencedatainflationspendingdataset

	thingspendingdataset <.. thinginflation
	thinginflation <.. thingreferencedata
	actorresearchergovernmentpublisher --- thingdatasetinterface
	thingreferencedata --- controladdreferencedatainflationspendingdataset
	thinginflation --- controladdreferencedatainflationspendingdataset
	thingspendingdataset --- controladdreferencedatainflationspendingdataset
	thingdatasetinterface --> controladdreferencedatainflationspendingdataset

@enduml