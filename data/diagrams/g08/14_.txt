
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Jquery Plugin" as thingjqueryplugin #grey
	entity "Core Data\nPackages" as thingcoredatapackages #grey
	actor "Developer" as actordeveloper
	boundary "Packages\nInterface" as thingpackagesinterface #grey
	control "Provided With\nA Jquery\nPlugin For\nCore Data\nPackages" as controlprovidejqueryplugin

	thingcoredatapackages <.. thingjqueryplugin
	actordeveloper --- thingpackagesinterface
	thingjqueryplugin --- controlprovidejqueryplugin
	thingcoredatapackages --- controlprovidejqueryplugin
	thingpackagesinterface --> controlprovidejqueryplugin

@enduml