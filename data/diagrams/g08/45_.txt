
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Datum" as thingdatum
	actor "Publisher" as actorpublisher
	boundary "Datum\nInterface" as thingdatuminterface #grey
	control "Time Check\nGood" as controlcheck
	control "Update Data" as controlupdatecheckdatum

	actorpublisher --- thingdatuminterface
	controlupdatecheckdatum --> controlcheck
	thingdatuminterface --> controlcheck
	thingdatum --- controlupdatecheckdatum
	thingdatuminterface --> controlupdatecheckdatum

@enduml