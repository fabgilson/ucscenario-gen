
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Ckan" as thingCKAN
	entity "Data Packages" as thingdatapackages #grey
	actor "Researcherpublisher" as actorResearcherPublisher
	boundary "Ckan\nInterface" as thingckaninterface #grey
	control "Publish Data\nPackages To\nCkan" as controlpublishdatapackagesCKAN

	thingCKAN <.. thingdatapackages
	actorResearcherPublisher --- thingckaninterface
	thingdatapackages --- controlpublishdatapackagesCKAN
	thingCKAN --- controlpublishdatapackagesCKAN
	thingckaninterface --> controlpublishdatapackagesCKAN

@enduml