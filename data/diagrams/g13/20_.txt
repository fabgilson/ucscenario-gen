
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Application" as thingapplication
	entity "Account" as thingaccount
	entity "Password" as thingpassword
	entity "Username" as thingusername
	entity "Email Address" as thingemailaddress #grey
	actor "Moderator" as actormoderator
	boundary "Username\nInterface" as thingusernameinterface #grey
	boundary "Address\nInterface" as thingaddressinterface #grey
	boundary "Application\nInterface" as thingapplicationinterface #grey
	boundary "Password\nInterface" as thingpasswordinterface #grey
	control "Create An\nAccount For\nThe Application\nEntering A\nUsername" as controlcreateaccountusername
	control "Create An\nAccount For\nThe Application\nEntering Email\nAddress" as controlcreateaccountemailaddress
	control "Create An\nAccount For\nThe Application\nEntering Name" as controlcreateaccountname
	control "Create An\nAccount For\nThe Application\nEntering A\nPassword" as controlcreateaccountpassword

	thingpassword <.. thingapplication
	thingemailaddress <.. thingapplication
	thingusername <.. thingapplication
	thingapplication <.. thingaccount
	actormoderator --- thingusernameinterface
	actormoderator --- thingaddressinterface
	actormoderator --- thingapplicationinterface
	actormoderator --- thingpasswordinterface
	thingusername --- controlcreateaccountusername
	thingusernameinterface --> controlcreateaccountusername
	thingapplication --- controlcreateaccountusername
	thingaccount --- controlcreateaccountusername
	thingemailaddress --- controlcreateaccountemailaddress
	thingaddressinterface --> controlcreateaccountemailaddress
	thingapplication --- controlcreateaccountemailaddress
	thingaccount --- controlcreateaccountemailaddress
	thingaccount --- controlcreateaccountname
	thingapplication --- controlcreateaccountname
	thingapplicationinterface --> controlcreateaccountname
	thingpassword --- controlcreateaccountpassword
	thingpasswordinterface --> controlcreateaccountpassword
	thingapplication --- controlcreateaccountpassword
	thingaccount --- controlcreateaccountpassword

@enduml