
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Item" as thingitem
	entity "Session" as thingsession
	actor "Estimator" as actorestimator
	boundary "Session\nInterface" as thingsessioninterface #grey
	boundary "Item\nInterface" as thingiteminterface #grey
	control "Try" as controltryseeitemestimatesession
	control "Estimate This\nSession" as controlestimatesession
	control "See All\nItems" as controlseeitem

	actorestimator --- thingsessioninterface
	actorestimator --- thingiteminterface
	thingsessioninterface --> controltryseeitemestimatesession
	controltryseeitemestimatesession --> controlestimatesession
	thingsession --- controlestimatesession
	thingsessioninterface --> controlestimatesession
	controltryseeitemestimatesession --> controlseeitem
	thingitem --- controlseeitem
	thingiteminterface --> controlseeitem

@enduml