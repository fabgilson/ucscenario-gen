
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "User Documentation" as thinguserdocumentation #grey
	entity "Website" as thingwebsite
	actor "User" as actoruser
	boundary "Website\nInterface" as thingwebsiteinterface #grey
	control "View User\nDocumentation For\nThe Website" as controlviewuserdocumentation

	thingwebsite <.. thinguserdocumentation
	actoruser --- thingwebsiteinterface
	thinguserdocumentation --- controlviewuserdocumentation
	thingwebsite --- controlviewuserdocumentation
	thingwebsiteinterface --> controlviewuserdocumentation

@enduml