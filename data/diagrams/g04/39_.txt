
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "User Error\nLogs" as thingusererrorlogs #grey
	actor "Admin" as actoradmin
	boundary "Logs\nInterface" as thinglogsinterface #grey
	control "View User\nError Logs" as controlviewusererrorlogs

	actoradmin --- thinglogsinterface
	thingusererrorlogs --- controlviewusererrorlogs
	thinglogsinterface --> controlviewusererrorlogs

@enduml