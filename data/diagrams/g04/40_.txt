
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Recycling Centers" as thingrecyclingcenters #grey
	entity "Platform" as thingplatform
	actor "Admin" as actoradmin
	boundary "Platform\nInterface" as thingplatforminterface #grey
	control "Onboard Recycling\nCenters On\nThe Platform" as controlonboardrecyclingcenters

	thingplatform <.. thingrecyclingcenters
	actoradmin --- thingplatforminterface
	thingrecyclingcenters --- controlonboardrecyclingcenters
	thingplatform --- controlonboardrecyclingcenters
	thingplatforminterface --> controlonboardrecyclingcenters

@enduml