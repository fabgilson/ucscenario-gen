
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Account" as thingaccount
	actor "User" as actoruser
	boundary "Account\nInterface" as thingaccountinterface #grey
	control "Create An\nAccount" as controlcreateaccount

	actoruser --- thingaccountinterface
	thingaccount --- controlcreateaccount
	thingaccountinterface --> controlcreateaccount

@enduml