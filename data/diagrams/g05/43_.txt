
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	actor "Data ,\nPublishing User" as actordatapublishinguser
	boundary "Dataset" as thingdatasetaddthatdatapublishinguser #grey
	control "That Public\nAdd" as controladdthat
	control "Hide" as controlhidedatasetaddthatdatapublishinguser

	actordatapublishinguser --- thingdatasetaddthatdatapublishinguser
	thingdatasetaddthatdatapublishinguser --> controladdthat
	controlhidedatasetaddthatdatapublishinguser --> controladdthat
	thingdatasetaddthatdatapublishinguser --> controlhidedatasetaddthatdatapublishinguser

@enduml