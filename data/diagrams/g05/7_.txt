
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Packager Support\nConstants" as thingpackagersupportconstants #grey
	actor "Data ,\nPublishing User" as actordatapublishinguser
	boundary "Constants\nInterface" as thingconstantsinterface #grey
	control "Have The\nPackager Support\nConstants" as controlhavepackagersupportconstants

	actordatapublishinguser --- thingconstantsinterface
	thingpackagersupportconstants --- controlhavepackagersupportconstants
	thingconstantsinterface --> controlhavepackagersupportconstants

@enduml