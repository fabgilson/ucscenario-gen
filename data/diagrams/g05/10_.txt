
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Json" as thingJSON
	entity "Datum" as thingdatum
	actor "Data ,\nPublishing User" as actordatapublishinguser
	boundary "Json\nInterface" as thingjsoninterface #grey
	control "Import Data\nIn Json" as controlimportdatumJSON

	thingJSON <.. thingdatum
	actordatapublishinguser --- thingjsoninterface
	thingdatum --- controlimportdatumJSON
	thingJSON --- controlimportdatumJSON
	thingjsoninterface --> controlimportdatumJSON

@enduml