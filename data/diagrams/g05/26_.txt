
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "City" as thingcity
	entity "Region" as thingregion
	actor "Api User" as actorapiuser
	boundary "City\nInterface" as thingcityinterface #grey
	boundary "Region\nInterface" as thingregioninterface #grey
	control "Bordering City" as controlbordercity
	control "Get" as controlgetborderbordercity
	control "Query A\nRegion" as controlquerygetborderregionregion
	control "Query City" as controlquerymoinsPRONmoinscity
	control "Bordering Regions" as controlborderregion

	actorapiuser --- thingcityinterface
	actorapiuser --- thingregioninterface
	thingcity --- controlbordercity
	thingcityinterface --> controlbordercity
	controlgetborderbordercity --> controlbordercity
	controlquerygetborderregionregion --> controlgetborderbordercity
	thingregioninterface --> controlgetborderbordercity
	thingregioninterface --> controlquerygetborderregionregion
	thingregion --- controlquerygetborderregionregion
	thingcity --- controlquerymoinsPRONmoinscity
	thingcityinterface --> controlquerymoinsPRONmoinscity
	thingregion --- controlborderregion
	thingregioninterface --> controlborderregion
	controlgetborderbordercity --> controlborderregion

@enduml