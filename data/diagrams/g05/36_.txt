
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Measure" as thingmeasure
	entity "Socioeconomic ,\nRelated Index" as thingsocioeconomicrelatedindex
	entity "Gini" as thingGINI
	actor "Api User" as actorapiuser
	boundary "Index\nInterface" as thingindexinterface #grey
	boundary "Gini\nInterface" as thingginiinterface #grey
	control "Normalise Socioeconomic,\nRelated Index" as controlnormalisesocioeconomicrelatedindex
	control "Normalise Measures\nBy Gini" as controlnormalisemeasure

	thingGINI <.. thingmeasure
	actorapiuser --- thingindexinterface
	actorapiuser --- thingginiinterface
	thingsocioeconomicrelatedindex --- controlnormalisesocioeconomicrelatedindex
	thingindexinterface --> controlnormalisesocioeconomicrelatedindex
	thingmeasure --- controlnormalisemeasure
	thingGINI --- controlnormalisemeasure
	thingginiinterface --> controlnormalisemeasure

@enduml